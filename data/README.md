# Directory organization

This directory should be organized like this:

```
├── BreastCancer
│   ├── GEO_HG_PPI.csv
│   ├── GEO_HG_PPI_scaled.csv (*)
│   ├── graph_patientScores.csv (*)
│   ├── graph_patientScores_random.csv (*)
│   ├── graph_patientScores_scaled.csv (*)
│   ├── HPRD_complete.csv (*)
│   ├── HPRD_ct.csv (*)
│   ├── HPRD_PPI.csv
│   ├── HPRD_random.csv (*)
│   ├── ind_fold.txt (*)
│   └── labels_GEO_HG.csv
├── CancerType
│   ├── Adj_Filtered_List_0Con.mat
│   ├── Adj_Spearman_6P.mat
│   ├── batch_sizes_PPI.csv (*)
│   ├── batch_sizes_Spearman.csv (*)
│   ├── Block_6PA.mat
│   ├── Block_6P.mat
│   ├── Block_PPI1.mat
│   ├── Block_PPIA.mat
│   ├── expr_PPI.csv (*)
│   ├── expr_Spearman.csv (*)
│   ├── labels_PPI.csv (*)
│   └── labels_Spearman.csv (*)
├── DREAM5
│   ├── adjacency_matrix.tab (*)
│   ├── expression_data.tsv
│   ├── expr_predictors_scaled.tab (*)
│   ├── folds.csv (*)
│   ├── network.tsv
│   └── target_scaled.tab (*)
├── F1000
│   ├── gene_regulatory_network_adjacency.pkl
│   ├── GSE92742_folds_restricted.csv (*)
│   ├── GSE92742_folds_restricted_prostate.csv (*)
│   ├── GSE92742_fully_restricted.csv (*)
│   ├── GSE92742_fully_restricted.hdf
│   ├── GSE92742_fully_restricted_prostate.csv (*)
│   ├── GSE92742_fully_restricted_prostate.hdf
│   ├── GSE92742_labels_restricted_prostate.csv (*)
│   ├── GSE92742_moa_restricted.csv (*)
│   ├── GSE92742_primarysite_restricted.csv (*)
│   ├── GSE92742_subtype_restricted.csv (*)
│   └── prostate_adjacency.pkl
├── Simulated
│   ├── complete_net.csv
│   ├── ct_net.csv
│   ├── expression.csv
│   ├── folds.csv
│   ├── graph_patient.csv
│   ├── graph_patient_random.csv
│   ├── log_targets.csv
│   ├── network.csv
│   ├── random_net.csv
│   ├── scaled_expression.csv
│   ├── scaled_targets.csv
└── └── target.csv
```


## Details on file availability

- *BreastCancer*: data available at http://mypathsem.bioinf.med.uni-goettingen.de/resources/glrp [accessed 2022-09-27]
- *CancerType*: data available at https://drive.google.com/drive/folders/1_Cnvab7mIwCrNJyY-J4aR2ck9i72KH8t?usp=sharing [accessed 2022-10-13]
- *DREAM5*: data available at https://www.synapse.org/#!Synapse:syn2787209 [accessed 2023-11-08] (training data / Network 1 - in silico / `net1_expression_data.tsv` and test data / `DREAM5_NetworkInference_GoldStandard_Network1 - in silico.tsv`)
- *F1000*: data available from authors of https://doi.org/10.1109/TCBB.2019.2910061
- *Additional data from authors* (preprocessed files, alternative networks, preprocessing script outputs, ...): (*) can be obtained from our dataset repository https://doi.org/10.57745/BZ0TTC in the file `breastcancer.zip` for BreastCancer, in the file `cancertype.zip` for CancerType, in the file `F1000.zip` for F1000, and in individual and documented files for Simulated.


