import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, roc_curve, auc, f1_score, balanced_accuracy_score, roc_auc_score
import pickle
import time

# A. Basic perceptron ###
## 1. PPI data
### Load expression data
pseudoCounts = pd.read_csv("../../data/CancerType/expr_PPI.csv", header=None)
pseudoCounts = pseudoCounts.values.astype(np.float64)

### Load target
Y = pd.read_csv("../../data/CancerType/labels_PPI.csv", header=None)
# Ybin = label_binarize(Y, classes=np.arange(0,33))
Y = np.ravel(Y)

### Load split indices and create folds
batch_sizes = pd.read_csv("../../data/CancerType/batch_sizes_PPI.csv", header=None)
batch_sizes = batch_sizes.values.astype(np.int64)

for fold in np.arange(0, 5):
    fold_size = batch_sizes[fold][0]
    if fold == 0:
        groups = 0*np.ones((batch_sizes[0][0],), dtype=int)
    else:
        ng = fold*np.ones((batch_sizes[fold][0],), dtype=int)
        groups = np.concatenate((groups, ng))
        
### Train
skf = GroupKFold(n_splits=5)
n_folds=5
accuracy = []
aucscore = []
balancedacc = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(skf.split(pseudoCounts, Y, groups)):
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = Y[train_index]
  ytest = Y[test_index]
  start = time.time()
  clf = MLPClassifier(random_state=903).fit(xtrain, ytrain)
  timetrain.append(time.time() - start)
  startpred = time.time()
  accuracy.append(clf.score(xtest, ytest))
  ypred = clf.predict_proba(xtest)
  aucscore.append(roc_auc_score(ytest, ypred, average = "macro", multi_class = "ovr"))
  ypred = clf.predict(xtest)
  balancedacc.append(balanced_accuracy_score(ytest, ypred))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore}

### Export
filehandler = open("../../results/CancerType/Perceptron_PPI_Scores_2022-03-24.obj", "wb")
pickle.dump(scores, filehandler)

## 2. Correlation data
### Load expression data
pseudoCounts = pd.read_csv("../../data/CancerType/expr_Spearman.csv", header=None)
pseudoCounts = pseudoCounts.values.astype(np.float64)

### Load target
Y = pd.read_csv("../../data/CancerType/labels_Spearman.csv", header=None)
# Ybin = label_binarize(Y, classes=np.arange(0,33))
Y = np.ravel(Y)

### Load split indices and create folds
batch_sizes = pd.read_csv("../../data/CancerType/batch_sizes_Spearman.csv", header=None)
batch_sizes = batch_sizes.values.astype(np.int64)
for fold in np.arange(0, 5):
    fold_size = batch_sizes[fold][0]
    if fold == 0:
        groups = 0*np.ones((batch_sizes[0][0],), dtype=int)
    else:
        ng = fold*np.ones((batch_sizes[fold][0],), dtype=int)
        groups = np.concatenate((groups, ng))

### Train
skf = GroupKFold(n_splits=5)
n_folds=5
accuracy = []
aucscore = []
balancedacc = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(skf.split(pseudoCounts, Y, groups)):
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = Y[train_index]
  ytest = Y[test_index]
  start = time.time()
  clf = MLPClassifier(random_state=903).fit(xtrain, ytrain)
  timetrain.append(time.time() - start)
  startpred = time.time()
  accuracy.append(clf.score(xtest, ytest))
  ypred = clf.predict_proba(xtest)
  aucscore.append(roc_auc_score(ytest, ypred, average = "macro", multi_class = "ovr"))
  ypred = clf.predict(xtest)
  balancedacc.append(balanced_accuracy_score(ytest, ypred))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore}

### Export
filehandler = open("../../results/CancerType/Perceptron_Spearman_Scores_2022-03-24.obj", "wb")
pickle.dump(scores, filehandler)
