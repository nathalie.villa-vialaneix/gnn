import pandas as pd
import pickle
import numpy as np

results = pd.read_csv("../../results/CancerType/RRandomForest_PPI_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/CancerType/RRandomForest_PPI_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/CancerType/RRandomForest_Spearman_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/CancerType/RRandomForest_Spearman_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/CancerType/RSVM_PPI_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/CancerType/RSVM_PPI_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/CancerType/RSVM_Spearman_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/CancerType/RSVM_Spearman_Scores_2023-01-26.obj", "wb")
pickle.dump(formatr, filehandler)

## notes
### unscaled for complete can not be computed
