# Script for running GNN on CancerType dataset using the architecture in Ramirez et al., 2020

import sys
sys.path.append('../GNN_spektral')
import os
import numpy as np
import tensorflow as tf
import spektral
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, roc_auc_score, confusion_matrix
from graph_lrp_master.lib import coarsening, graph
from models import GNNLearning
import pickle
import argparse
import scipy.io as sio
import h5py

parser = argparse.ArgumentParser(description='GNN applied to cancer type dataset.')
parser.add_argument('--graph', type=str, help='Name of the file containing the graph.')
parser.add_argument('--expr', type=str, help='Name of the file containing the gene expression.')

# Get the arguments from the command line
args = parser.parse_args()

# Parameters

params = dict()
params['epochs'] = 20 # number of training epochs
params['batch_size'] = 200
if (args.expr == 'Block_6PA') or (args.expr == 'Block_PPIA'):
    params['learning_rate'] = 0.005 # Initial learning rate
if (args.expr == 'Block_6P') or (args.expr == 'Block_PPI1'):
    params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 1 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['decay_rate'] = 0.95 # Base of exponential decay. No decay with 1.
#params['decay_steps'] = 17.7 #Number of steps after which the learning rate decays.
params['reg_lambda'] = 0 # regularization parameter
params['momentum'] = 0 # 0 indicates no momentum
params['pool1'] = 'apool1'
params['activation'] = "relu"
params['last_activation'] = "softmax"
params['conv'] = "chebnet" # convolutional layer

# Parameters of the GNN architecture (corresponds to the architecture of Chereda)
params['F'] = [1] # Number of graph convolutional filters
params['K'] = [1] # Polynomial orders
params['p'] = [2] # Pooling sizes
params['M'] = [1024, 34] # Output dimensionality of fully connected layers

# Loading graph

test = sio.loadmat("../../data/CancerType/"+args.graph+".mat")
row = test['row'].astype(np.float32)
col = test['col'].astype(np.float32)
value = test['value'].astype(np.float32)
M, k = row.shape
row = np.array(row)
row = row.reshape(k)
row = row.ravel()
col = np.array(col)
col = col.reshape(k)
col = col.ravel()
value = np.array(value)
value = value.reshape(k)
value = value.ravel()
#### changer valeur 4444 selon jeu de données
if args.graph == "Adj_Filtered_List_0Con":
    A = csr_matrix((value, (row, col)), shape=(4444, 4444))
if args.graph == "Adj_Spearman_6P":
    A = csr_matrix((value, (row, col)), shape=(3866, 3866))
# graph coarsening
graphs, perm = coarsening.coarsen(A, levels=len(params['p']), self_connections=True)
L = [graph.laplacian(A, normalized=True,renormalized=True) for A in graphs]
L = [spektral.utils.sparse.sp_matrix_to_sp_tensor(A) for A in L] # convert to sparse tensor
del test
del A
del row
del col
del value

# Loading gene expression and label (cancer classes)

Data = sio.loadmat("../../data/CancerType/"+args.expr+".mat")
Data1 = Data['Block'][0,0]
Data2 = Data['Block'][0,1]
Data3 = Data['Block'][0,2]
Data4 = Data['Block'][0,3]
Data5 = Data['Block'][0,4]
D1 = Data1['D'].astype(np.float32)
D2 = Data2['D'].astype(np.float32)
D3 = Data3['D'].astype(np.float32)
D4 = Data4['D'].astype(np.float32)
D5 = Data5['D'].astype(np.float32)
L1 = Data1['L'].astype(np.float32)
L2 = Data2['L'].astype(np.float32)
L3 = Data3['L'].astype(np.float32)
L4 = Data4['L'].astype(np.float32)
L5 = Data5['L'].astype(np.float32)

X = np.transpose(np.hstack((D1,D2,D3,D4,D5)))
X = coarsening.perm_data(X, perm)
y = (np.vstack((L1,L2,L3,L4,L5)))
y = y.ravel()
n_classes = len(np.unique(y))

# Creating a custom Spektral dataset
class BreastCancerDataset(spektral.data.dataset.Dataset):
    def read(self):
        self.a = L[0]
        return [spektral.data.graph.Graph(x=X[i].reshape(X[i].size,1), y=y[i]) for i in range(X.shape[0])]
        
data = BreastCancerDataset()

# Cross-validation
n_folds = 5
acc_test = np.zeros(n_folds)
acc_bal_test = np.zeros(n_folds)
roc_auc_test = np.zeros(n_folds)
acc_train = np.zeros(n_folds)
acc_bal_train = np.zeros(n_folds)
conf_matrix = np.zeros((n_classes, n_classes, n_folds))
fit_time = np.zeros(n_folds)
score_time = np.zeros(n_folds)

# define groups in order to use the same folds as in Ramirez et al.
g1 = 0*np.ones((D1.shape[1],), dtype=int)
g2 = 1*np.ones((D2.shape[1],), dtype=int)
g3 = 2*np.ones((D3.shape[1],), dtype=int)
g4 = 3*np.ones((D4.shape[1],), dtype=int)
g5 = 4*np.ones((D5.shape[1],), dtype=int)
groups = np.concatenate((g1,g2,g3,g4,g5))

skf = GroupKFold(n_splits=n_folds)
for fold, (train_index, test_index) in enumerate(skf.split(X, y, groups)):
    print(f"Fold = {fold}")

    data_train = data[train_index]
    data_test = data[test_index]
    
    params['decay_steps'] = len(data_train) / params['batch_size'] #Number of steps after which the learning rate decays.

    # Training GNN and Testing
    fit_time[fold], score_time[fold], y_test_true, y_test_pred, y_test_score, y_train_true, y_train_pred = GNNLearning(data_train, data_test, L, params)
    
    acc_test[fold] = accuracy_score(y_test_true, y_test_pred)
    acc_bal_test[fold] = balanced_accuracy_score(y_test_true, y_test_pred)
    roc_auc_test[fold] = roc_auc_score(y_test_true, y_test_score, average = "macro", multi_class="ovr")
    conf_matrix[:,:,fold] = confusion_matrix(y_test_true, y_test_pred)
    acc_train[fold] = accuracy_score(y_train_true, y_train_pred)
    acc_bal_train[fold] = balanced_accuracy_score(y_train_true, y_train_pred)
    
    print(f"Train:  Acc = {acc_train[fold]:.3f} ; Balanced acc = {acc_bal_train[fold]:.3f}")
    print(f"Test:  Acc = {acc_test[fold]:.3f} ; Balanced acc = {acc_bal_test[fold]:.3f} ; AUROC = {roc_auc_test[fold]:.3f}")
    

# Save results
score = {'fit_time':fit_time,'score_time':score_time,'test_accuracy':acc_test,'test_balanced_accuracy':acc_bal_test,'test_roc_auc':roc_auc_test, 'conf_matrix': conf_matrix, 'train_accuracy':acc_train, 'train_balanced_accuracy':acc_bal_train}

results_filename ='../../results/CancerType/GNN_Ramirez_'+args.graph+'_'+args.expr+'_Scores_2023-11-09.obj'
with open(results_filename, 'wb') as handle:
    pickle.dump(score, handle)
