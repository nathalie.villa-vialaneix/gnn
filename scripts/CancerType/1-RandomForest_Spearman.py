import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, roc_curve, auc, f1_score, balanced_accuracy_score, roc_auc_score
import pickle
import time

## 2. with Spearman correlation data ###
## Load expression data
pseudoCounts = pd.read_csv("../../data/CancerType/expr_Spearman.csv", header=None)
pseudoCounts = pseudoCounts.values.astype(np.float64)

### Load target
Y = pd.read_csv("../../data/CancerType/labels_Spearman.csv", header=None)
Y = np.ravel(Y)

### Load split indices and create folds
batch_sizes = pd.read_csv("../../data/CancerType/batch_sizes_Spearman.csv", header=None)
batch_sizes = batch_sizes.values.astype(np.int64)
for fold in np.arange(0, 5):
    fold_size = batch_sizes[fold][0]
    if fold == 0:
        groups = 0*np.ones((batch_sizes[0][0],), dtype=int)
    else:
        ng = fold*np.ones((batch_sizes[fold][0],), dtype=int)
        groups = np.concatenate((groups, ng))

### Train
skf = GroupKFold(n_splits=5)
n_folds=5
accuracy = []
train_acc = []
aucscore = []
balancedacc = []
train_bal_acc = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(skf.split(pseudoCounts, Y, groups)):
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = Y[train_index]
  ytest = Y[test_index]
  start = time.time()
  clf = RandomForestClassifier(n_estimators=500, min_samples_split=2, random_state=1647, bootstrap=False, n_jobs=-1).fit(xtrain, ytrain)
  timetrain.append(time.time() - start)
  startpred = time.time()
  accuracy.append(clf.score(xtest, ytest))
  train_acc.append(clf.score(xtrain, ytrain))
  ypred = clf.predict_proba(xtest)
  aucscore.append(roc_auc_score(ytest, ypred, average = "macro", multi_class = "ovr"))
  ypred = clf.predict(xtest)
  ypred_train = clf.predict(xtrain)
  balancedacc.append(balanced_accuracy_score(ytest, ypred))
  train_bal_acc.append(balanced_accuracy_score(ytrain, ypred_train))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore, 'train_accuracy': train_acc, 'train_balanced_accuracy': train_bal_acc}

### Export
filehandler = open("../../results/CancerType/RF_Spearman_Scores_2022-03-24.obj", "wb") 
pickle.dump(scores, filehandler)
