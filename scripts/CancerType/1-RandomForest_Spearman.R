# Settings #####
setwd("../..")
library("randomForest")
library("pROC")
library("profmem")
cur_date <- "2023-01-26"

# Loading data (Spearman) ####
# loading expression data: samples in columns
expr_file <- "data/CancerType/expr_Spearman.csv"
expr <- read.table(expr_file, header = FALSE, sep = ",")

# loading labels
labels <- read.table("data/CancerType/labels_Spearman.csv", header = FALSE)
labels <- unlist(labels)
labels <- as.factor(labels)

# loading folds
foldsizes <- read.table("data/CancerType/batch_sizes_Spearman.csv", 
                        header = FALSE)
foldsizes <- unlist(foldsizes)

# CV training
out_file <- paste0("RRandomForest_Spearman_Scores_", cur_date, ".csv")
out_file <- file.path("results/CancerType", out_file)
mem_file <- gsub("\\.csv", "\\.dat", gsub("Scores", "Mem", out_file))

if (!file.exists(out_file)) {
  set.seed(2807)
  start <- 1
  train_time <- rep(0, 5)
  test_time <- rep(0, 5)
  accuracy <- rep(0, 5)
  bal_accuracy <- rep(0, 5)
  train_accuracy <- rep(0, 5)
  train_bal_acc <- rep(0, 5)
  auc_roc <- rep(0, 5)
  
  totalmem <- total(profmem({
    for (ind in 1:5) {
      cat("Processing fold...", ind)
      train_time[ind] <- system.time({
        training_obs <- setdiff(1:nrow(expr), start:(start + foldsizes[ind] - 1))
        start <- start + foldsizes[ind]
        cur_mod <- randomForest(expr[training_obs, ], labels[training_obs], 
                                replace = FALSE, sampsize = length(training_obs),
                                nodesize = 1)
      })[3]
    
      test_time[ind] <- system.time({
        testing_obs <- (start - foldsizes[ind]):(start - 1)
        cur_pred <- predict(cur_mod, expr[testing_obs, ])
        cur_pred_train <- predict(cur_mod, expr[training_obs, ])
        accuracy[ind] <- mean(cur_pred == labels[testing_obs])
        train_accuracy[ind] <- mean(cur_pred_train == labels[training_obs])
        conf_table <- table(cur_pred, labels[testing_obs])
        bal_accuracy[ind] <- mean(diag(conf_table) / apply(conf_table, 2, sum))
        conf_table <- table(cur_pred_train, labels[training_obs])
        train_bal_acc[ind] <- mean(diag(conf_table) / apply(conf_table, 2, sum))
        cur_scores <- predict(cur_mod, expr[testing_obs, ], type = "prob")
        aucroc <- multiclass.roc(labels[testing_obs], cur_scores)
        auc_roc[ind] <- as.numeric(aucroc$auc)
      })[3]
    
    }
  }))
  
  all_res <- cbind(accuracy, bal_accuracy, train_accuracy, train_bal_acc,
                   auc_roc, train_time, test_time)
  names(all_res) <- c("accuracy", "balanced_accuracy", "training_accuracy",
                      "training_balanced_accuracy", "auc_roc", "training_time", 
                      "testing_time")
  
  write.csv(all_res, file = out_file, row.names = FALSE)
  write.table(totalmem, file = mem_file, row.names = FALSE, col.names = FALSE)
}
