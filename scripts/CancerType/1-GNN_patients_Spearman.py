#!/usr/bin/env python
# coding: utf-8


# Training of graph neural network (GNN) whose convolution is done over the patients.
# Training of dense neural network (DNN) is also included.

import os
os.getcwd()


import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, roc_curve, auc, f1_score, balanced_accuracy_score, roc_auc_score
import pickle
import time
import argparse

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping
from scipy import sparse
from sklearn.metrics.pairwise import euclidean_distances

import sys
sys.path.append('../GNN_keras')
from models_gnn_patients import *

#tf.keras.utils.set_random_seed(seed=100)
tf.random.set_seed(seed=100)

# Parameters
parser = argparse.ArgumentParser(description='GNN applied to cancer type dataset.')
parser.add_argument('--modelType', type=str, help='GNN or DNN')
parser.add_argument('--randomGraph', type=int, help='indicates if random graph is used (2: complete graph; 1: random graph; 0: graph estimated from data).')

# Get the arguments from the command line
args = parser.parse_args()
modelType=args.modelType
randomGraph=args.randomGraph
print(modelType)
print(randomGraph)


## 2. with Spearman correlation data ###
## Load expression data
pseudoCounts = pd.read_csv("../../data/CancerType/expr_Spearman.csv", header=None)
pseudoCounts = pseudoCounts.values.astype(np.float64)

### Load target
Y = pd.read_csv("../../data/CancerType/labels_Spearman.csv", header=None)
Y = np.ravel(Y)
num_classes=len(np.unique(Y))

### Load split indices and create folds
batch_sizes = pd.read_csv("../../data/CancerType/batch_sizes_Spearman.csv", header=None)
batch_sizes = batch_sizes.values.astype(np.int64)
for fold in np.arange(0, 5):
    fold_size = batch_sizes[fold][0]
    if fold == 0:
        groups = 0*np.ones((batch_sizes[0][0],), dtype=int)
    else:
        ng = fold*np.ones((batch_sizes[fold][0],), dtype=int)
        groups = np.concatenate((groups, ng))


# Individual similarity graph
if modelType == "GNN":
    if randomGraph == 0:
        A=euclidean_distances(pseudoCounts)
        A=A/np.max(A)
        A=1-A
        np.fill_diagonal(A,0)
        A[A<np.quantile(A,0.998)]=0
        A[A>=np.quantile(A,0.998)]=1
        file_graph = "../../data/CancerType/graph_patient_Spearman.csv"
        pd.DataFrame(A).to_csv(file_graph, header=False, index=False)

    if randomGraph == 1:
        file_graph = "../../data/CancerType/graph_patient_Spearman_random.csv"
    
        A = pd.read_csv(file_graph,header=None)


    # Make data for the graph convolutional network
    edges=sparse.csr_matrix(A)
    edges=np.stack((edges.nonzero()[0],edges.nonzero()[1]),axis=0)
    print(edges[:,0:10])
    # Create an edge weights array of ones.
    edge_weights = tf.ones(shape=edges.shape[1])
    # Create a node features array of shape [num_nodes, num_features].
    node_features = tf.cast(pseudoCounts, dtype=tf.dtypes.float32)
    # Create graph info tuple with node_features, edges, and edge_weights.
    graph_info = (node_features, edges, edge_weights)

    print("Edges shape:", edges.shape)
    print("Nodes shape:", node_features.shape)


# Parameters
params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 200
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 0 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['reg_lambda'] = 1e-4 # regularization parameter
params['F'] = 100 # Number of graph convolutional filters
params['num_classes'] = num_classes
params['activation'] = "relu"
params['last_activation'] = "softmax"


### Train
skf = GroupKFold(n_splits=5)
n_folds=5
accuracy = []
aucscore = []
balancedacc = []
timetrain = []
timepred = []
train_accuracy = []
train_balanced_accuracy = []
for fold, (train_index, test_index) in enumerate(skf.split(pseudoCounts, Y, groups)):
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = Y[train_index]
  ytest = Y[test_index]
  start = time.time()
  if modelType=="GNN":
        model = run_GNN_patient(train_index, ytrain, graph_info, params)
  elif modelType=="DNN":
        model = run_DNN(xtrain, ytrain, params)
  timetrain.append(time.time() - start)
  startpred = time.time()
  if modelType=="GNN":
        ypred_proba=model.predict(x=np.array(test_index), verbose=0)
        train_ypred_proba=model.predict(x=np.array(train_index), verbose=0)
  elif modelType=="DNN":
        ypred_proba=model.predict(x=xtest, verbose=0)
        train_ypred_proba=model.predict(x=xtrain, verbose=0)
  ypred=np.argmax(ypred_proba, axis=1)
  train_ypred=np.argmax(train_ypred_proba, axis=1)
  train_accuracy.append(accuracy_score(ytrain, train_ypred))
  train_balanced_accuracy.append(balanced_accuracy_score(ytrain, train_ypred))
    
  accuracy.append(accuracy_score(ytest, ypred))
  aucscore.append(roc_auc_score(ytest, ypred_proba, average = "macro", multi_class = "ovr"))
  balancedacc.append(balanced_accuracy_score(ytest, ypred))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore,'train_accuracy': train_accuracy, 'train_balanced_accuracy': train_balanced_accuracy}


### Export
filehandler = open("../../results/CancerType/"+modelType+"_patient_Spearman_Scores_2023-11-09.obj", "wb") 
pickle.dump(scores, filehandler)
filehandler.close()





