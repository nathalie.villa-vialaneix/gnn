#! /bin/bash
mprof run -T 5 -C -o mprof_perceptron_ppi.dat 1-Perceptron_PPI.py
mprof run -T 5 -C -o mprof_perceptron_spearman.dat 1-Perceptron_Spearman.py
mprof run -T 5 -C -o mprof_randomforest_ppi.dat 1-RandomForest_PPI.py
mprof run -T 5 -C -o mprof_randomforest_spearman.dat 1-RandomForest_Spearman.py
mprof run -T 5 -o mprof_svm_ppi.dat 1-SVM_PPI.py
mprof run -T 5 -o mprof_svm_spearman.dat 1-SVM_Spearman.py

R --vanilla -s -f 1-RandomForest_PPI.R
R --vanilla -s -f 1-RandomForest_Spearman.R
R --vanilla -s -f 1-SVM_PPI.R
R --vanilla -s -f 1-SVM_Spearman.R
python3 2-formatCSV2OJB.py

mprof run -T 5 -C -o mprof_gnnpatient_PPI.dat 1-GNN_patients_PPI.py --modelType GNN --randomGraph 0
mprof run -T 5 -C -o mprof_gnnpatient_Spearman.dat 1-GNN_patients_Spearman.py --modelType GNN --randomGraph 0
mprof run -T 5 -C -o mprof_gnnpatient_PPI_dnn.dat 1-GNN_patients_PPI.py --modelType DNN --randomGraph 0
mprof run -T 5 -C -o mprof_gnnpatient_Spearman_dnn.dat 1-GNN_patients_Spearman.py --modelType DNN --randomGraph 0
mprof run -T 5 -C -o mprof_gnn_chereda_PPI1.dat 1-GNNChereda.py --graph Adj_Filtered_List_0Con --expr Block_PPI1
mprof run -T 5 -C -o mprof_gnn_chereda_PPIA.dat 1-GNNChereda.py --graph Adj_Filtered_List_0Con --expr Block_PPIA
mprof run -T 5 -C -o mprof_gnn_chereda_spearman.dat 1-GNNChereda.py --graph Adj_Spearman_6P --expr Block_6P
mprof run -T 5 -C -o mprof_gnn_ramirez_PPI1.dat 1-GNNRamirez.py --graph Adj_Filtered_List_0Con --expr Block_PPI1
mprof run -T 5 -C -o mprof_gnn_ramirez_PPIA.dat 1-GNNRamirez.py --graph Adj_Filtered_List_0Con --expr Block_PPIA
mprof run -T 5 -C -o mprof_gnn_ramirez_spearman.dat 1-GNNRamirez.py --graph Adj_Spearman_6P --expr Block_6P
