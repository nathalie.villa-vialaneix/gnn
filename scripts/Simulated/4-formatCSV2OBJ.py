import pandas as pd
import pickle
import numpy as np

results = pd.read_csv("../../results/Simulated/glmgraphScores_P10_2023-02-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/Simulated/glmgraphScores_P10_2023-02-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/Simulated/glmgraph_CTScores_P10_2023-02-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/Simulated/glmgraph_CTScores_P10_2023-02-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/Simulated/glmgraph_randomScores_P10_2023-02-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/Simulated/glmgraph_randomScores_P10_2023-02-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/Simulated/glmgraph_completeScores_P10_2023-02-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/Simulated/glmgraph_completeScores_P10_2023-02-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/Simulated/RRandomForest_Scores_P10_2023-02-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/Simulated/RRandomForest_Scores_P10_2023-02-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/Simulated/RSVM_Scores_P10_2023-02-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/Simulated/RSVM_Scores_P10_2023-02-09.obj", "wb") 
pickle.dump(formatr, filehandler)
filehandler.close()
