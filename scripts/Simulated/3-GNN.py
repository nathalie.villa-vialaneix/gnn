# Script for running GNN on Simulated dataset.

import sys
sys.path.append('../GNN_spektral')
import os
import numpy as np
import tensorflow as tf
import spektral
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score
from graph_lrp_master.lib import coarsening, graph
from models import GNNReg
import pickle
import argparse
import scipy.io as sio
import h5py


parser = argparse.ArgumentParser(description='GNN applied to simulated dataset.')
parser.add_argument('--target', type=str, help='indicates the target.')
parser.add_argument('--graph', type=str, help='indicates the name of the file containing the graph.')

# Get the arguments from the command line
args = parser.parse_args()

# Parameters

params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 100
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 1 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['decay_rate'] = 0.95 # Base of exponential decay. No decay with 1.
params['reg_lambda'] = 1e-4 # regularization parameter
params['momentum'] = 0 # 0 indicates no momentum
params['activation'] = "relu"
params['last_activation'] = "linear"
params['conv'] = "chebnet" # convolutional layer

# Parameters of the GNN architecture (corresponds to the architecture of Chereda)
params['F'] = [32] # Number of graph convolutional filters
params['K'] = [8] # Polynomial orders
params['p'] = [2] # Pooling sizes
params['M'] = [128, 1] # Output dimensionality of fully connected layers
params['pool1'] = 'mpool1'

# Loading data

pseudoCounts = pd.read_csv("../../data/Simulated/scaled_expression.csv")
pseudoCounts_names = pseudoCounts.columns.values
pseudoCounts = pseudoCounts.values.astype(np.float64)

labels = pd.read_csv("../../data/Simulated/scaled_targets.csv")
labels = labels.pop(args.target)
y = labels.values

if args.graph == "network":
    A = pd.read_csv("../../data/Simulated/network.csv")
    A_names = A.columns.values
    A = A.values
    A = A+A.T
    for i in range(A.shape[0]):
        A[i,i] = 0
    A = csr_matrix(A)
else:
    A = pd.read_csv("../../data/Simulated/"+args.graph+".csv", sep=" ")
    A_names = A.columns.values
    A = A.values
    A = csr_matrix(A)
    

ind = [key for key, val in enumerate(A_names) if val in set(pseudoCounts_names)]
ind = np.atleast_2d(ind)
A = A[ind.T,ind]

# remove nodes with degree 0
ind_degree = np.where(A.sum(axis=0) - A.diagonal() != 0)
ind_degree = ind_degree[1]
ind_degree = np.atleast_2d(ind_degree)
A = A[ind_degree.T,ind_degree]
pseudoCounts = pseudoCounts[:,ind_degree[0]]


# graph coarsening
graphs, perm = coarsening.coarsen(A, levels=len(params['p']), self_connections=False)
L = [spektral.layers.ChebConv.preprocess(A) for A in graphs]
L = [spektral.utils.sparse.sp_matrix_to_sp_tensor(A) for A in L] # convert to sparse tensor

X = coarsening.perm_data(pseudoCounts, perm)

del A
del graphs

# Creating a custom Spektral dataset
class BreastCancerDataset(spektral.data.dataset.Dataset):
    def read(self):
        self.a = L[0]
        return [spektral.data.graph.Graph(x=X[i].reshape(X[i].size,1), y=y[i]) for i in range(X.shape[0])]
        
data = BreastCancerDataset()

# Cross-validation
n_folds = 10
mse_test = np.zeros(n_folds)
explainvar_test = np.zeros(n_folds)
r2_test = np.zeros(n_folds)
mse_train = np.zeros(n_folds)
explainvar_train = np.zeros(n_folds)
r2_train = np.zeros(n_folds)
fit_time = np.zeros(n_folds)
score_time = np.zeros(n_folds)

skf = KFold(n_splits=10, shuffle=True, random_state=1024)
for fold, (train_index, test_index) in enumerate(skf.split(X, y)):
    print(f"Fold = {fold}")

    data_train = data[train_index]
    data_test = data[test_index]
    
    params['decay_steps'] = len(data_train) / params['batch_size'] # Number of steps after which the learning rate decays.

    # Training GNN and Testing
    fit_time[fold], score_time[fold], y_test_true, y_test_pred, y_train_true, y_train_pred  = GNNReg(data_train, data_test, L, params)
    
    mse_test[fold] = mean_squared_error(y_test_true, y_test_pred)
    explainvar_test[fold] = explained_variance_score(y_test_true, y_test_pred)
    r2_test[fold] = r2_score(y_test_true, y_test_pred)
    
    mse_train[fold] = mean_squared_error(y_train_true, y_train_pred)
    explainvar_train[fold] = explained_variance_score(y_train_true, y_train_pred)
    r2_train[fold] = r2_score(y_train_true, y_train_pred)
    
    print(f"Train:  Mse = {mse_train[fold]:.3f} ; Explained var = {explainvar_train[fold]:.3f} ; R2 = {r2_train[fold]:.3f}")
    print(f"Test:  Mse = {mse_test[fold]:.3f} ; Explained var = {explainvar_test[fold]:.3f} ; R2 = {r2_test[fold]:.3f}")

# Save results
score = {'fit_time':fit_time,'score_time':score_time,'test_mean_squared_error':mse_test,'test_r2':r2_test,'test_explained_variance':explainvar_test,'train_mean_squared_error':mse_train,'train_r2':r2_train,'train_explained_variance':explainvar_train}
results_filename ='../../results/Simulated/GNN_'+args.graph+'_Scores_' + args.target + '_2023-11-09.obj'
with open(results_filename, 'wb') as handle:
    pickle.dump(score, handle)
