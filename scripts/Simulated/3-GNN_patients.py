#!/usr/bin/env python
# coding: utf-8


# Training of graph neural network (GNN) whose convolution is done over the patients.
# Training of dense neural network (DNN) is also included.


import os
os.getcwd()


import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score
import pickle
import time
import argparse

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping
from scipy import sparse
from sklearn.metrics.pairwise import euclidean_distances

import sys
sys.path.append('../GNN_keras')
from models_gnn_patients import *

tf.keras.utils.set_random_seed(seed=100)

print(tf.__version__)



# Parameters
parser = argparse.ArgumentParser(description='GNN applied to simulated dataset.')
parser.add_argument('--modelType', type=str, help='GNN or DNN')
parser.add_argument('--scaled_targets', type=str, help='True or False')
parser.add_argument('--target', type=str, help='P5, P10, P12 or P17')
parser.add_argument('--randomGraph', type=int, help='indicates if random graph is used (1: random graph; 0: graph estimated from data).')

# Get the arguments from the command line
args = parser.parse_args()
modelType=args.modelType
scaled_targets=args.scaled_targets
target=args.target
randomGraph=args.randomGraph
print(modelType)
print(scaled_targets)
print(target)
print(randomGraph)


# A. Basic perceptron ###
pseudoCounts = pd.read_csv("../../data/Simulated/scaled_expression.csv")
pseudoCounts = pseudoCounts.values.astype(np.float64)

## 1. with unscaled data / P5 predicted ###
if scaled_targets=="False":
    labels = pd.read_csv("../../data/Simulated/target.csv")
else:
    labels = pd.read_csv("../../data/Simulated/scaled_targets.csv")    

labels = labels.pop(target)


# Make similarity graph between patients
if modelType=="GNN":
    if randomGraph == 0:
        A=euclidean_distances(pseudoCounts)
        A=A/np.max(A)
        A=1-A
        np.fill_diagonal(A,0)
        A[A<np.quantile(A,0.998)]=0
        A[A>=np.quantile(A,0.998)]=1
        file_graph = "../../data/Simulated/graph_patient.csv"
        pd.DataFrame(A).to_csv(file_graph, header=False, index=False)

    if randomGraph == 1:
        file_graph = "../../data/Simulated/graph_patient_random.csv"
    
        A = pd.read_csv(file_graph,header=None)



    # Make data for the graph convolutional network
    edges=sparse.csr_matrix(A)
    edges=np.stack((edges.nonzero()[0],edges.nonzero()[1]),axis=0)
    print(edges[:,0:10])
    # Create an edge weights array of ones.
    edge_weights = tf.ones(shape=edges.shape[1])
    # Create a node features array of shape [num_nodes, num_features].
    node_features = tf.cast(pseudoCounts, dtype=tf.dtypes.float32)
    # Create graph info tuple with node_features, edges, and edge_weights.
    graph_info = (node_features, edges, edge_weights)

    print("Edges shape:", edges.shape)
    print("Nodes shape:", node_features.shape)


# Parameters
params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 32
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 0 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['reg_lambda'] = 1e-4 # regularization parameter
params['F'] = 10 # Number of graph convolutional filters
params['num_classes'] = 1 # 1 if regression
params['activation'] = "linear"
params['last_activation'] = "linear"


### Train
all_folds = KFold(n_splits=10, shuffle=True, random_state=1024)

mse = []
mse_train = []
explainvar = []
explainvar_train = []
r2 = []
r2_train = []
pred = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(all_folds.split(pseudoCounts, labels)):
  import gc
  gc.collect()
    
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = labels[train_index]
  ytest = labels[test_index]
  start = time.time()
  if modelType=="GNN":
        model = run_GNN_patient(train_index, ytrain, graph_info, params)
  elif modelType=="DNN":
        model = run_DNN(xtrain, ytrain, params)
  timetrain.append(time.time() - start)
  startpred = time.time()
  if modelType=="GNN":
        ypred=model.predict(x=np.array(test_index), verbose=0)
        ypred_train=model.predict(x=np.array(train_index), verbose=0)        
  elif modelType=="DNN":  
        ypred=model.predict(x=xtest, verbose=0)   
        ypred_train=model.predict(x=xtrain, verbose=0)           
  pred.append(ypred)
  
  mse_train.append(mean_squared_error(ytrain, ypred_train))
  explainvar_train.append(explained_variance_score(ytrain, ypred_train))
  r2_train.append(r2_score(ytrain, ypred_train))
  
  mse.append(mean_squared_error(ytest, ypred))
  explainvar.append(explained_variance_score(ytest, ypred))
  r2.append(r2_score(ytest, ypred))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_mean_squared_error':mse, 'test_explained_variance':explainvar, 'test_r2':r2,'train_mean_squared_error': mse_train, 'train_explained_variance': explainvar_train, 'train_r2': r2_train}


### Export
if scaled_targets=="False":
    if randomGraph == 0:
        filehandler = open("../../results/Simulated/"+modelType+"_patient_Scores_"+target+\
        	"_2023-11-09.obj", "wb")
    else:
        filehandler = open("../../results/Simulated/"+modelType+"_patient_Scores_"+target+\
        	"_randomGraph_2023-11-09.obj","wb")
else:
    if randomGraph == 0:
        filehandler = open("../../results/Simulated/"+modelType+"_patient_Scores_"+target+\
        	"_scaled_2023-11-09.obj", "wb")
    else:
        filehandler = open("../../results/Simulated/"+modelType+"_patient_Scores_"+target+\
        	"_scaled_randomGraph_2023-11-09.obj", "wb") 
    

pickle.dump(scores, filehandler)
filehandler.close()




