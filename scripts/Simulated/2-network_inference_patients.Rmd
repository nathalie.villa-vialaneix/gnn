---
title: "Alternative networks for Simulated"
author: "Nathalie Vialaneix"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    numbered_sections: yes
    self_contained: yes
    theme: journal
    toc: yes
    toc_float:
      collapsed: no
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

This file aims at generating two types of networks for Simulated:

* first, a network inferred from expression data (with GGM if relevant) with
the same density as the real network

* second, a random network with the same characteristics (degree distribution)
as the real network

Both alternatives are saved in files formatted as `network.csv` (full 0/1
adjacency matrices). 

The script uses the following packages:
```{r loadLib, message=FALSE}
library("igraph")
library("huge")
```



# Loading and preparing data

```{r loadData}
# load patient similarity graph
patient_adj <- read.table("../../data/Simulated/graph_patient.csv", header = F,
                      sep = ",")
patient_adj <- as.matrix(patient_adj)
patient_net <- graph_from_adjacency_matrix(patient_adj, mode = "undirected")
patient_net
```

```{r randomNetwork}
set.seed(812)
m <- ecount(patient_net)
repeat {
  random_net <- rewire(patient_net, keeping_degseq(niter = 100 * m))

  if (is.simple(random_net)) break
}
```

```{r randomNodes}
set.seed(1305)
permutation <- sample(1:vcount(patient_net))
V(random_net)$name <- V(random_net)$name[permutation]
```

I was not able to obtain a connected network from this procedure. However, the degree of connectedness seems to be good enough:
```{r cc}
random_cc <- clusters(random_net)
random_cc$csize
``` 

Connected components not connected to the largest connected components are re-connected to the largest lcc with the addition of a few random connections:

```{r connectRandom}
set.seed(1525)
lcc_random <- which(random_cc$membership == which.max(random_cc$csize))
for (noclust in setdiff(1:length(random_cc$csize), which.max(random_cc$csize))) {
  cur_nodes <- which(random_cc$membership == noclust)
  to_connect <- c(sample(names(cur_nodes), 1), sample(names(lcc_random), 1))
  random_net <- add_edges(random_net, to_connect)
}
``` 

```{r save}
random_adj=as.matrix(as.matrix(random_net))
write.table(random_adj,file="../../data/Simulated/graph_patient_random.csv", col.names = F,
                      row.names=F, sep = ",")

``` 



# Session information

```{r sessionInfo}
sessionInfo()
```

