import pandas as pd
import numpy as np
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score
import pickle
import time

# A. Basic perceptron ###
pseudoCounts = pd.read_csv("../../data/Simulated/scaled_expression.csv")
pseudoCounts = pseudoCounts.values.astype(np.float64)

## 3. with scaled data / P10 predicted ###
labels = pd.read_csv("../../data/Simulated/scaled_targets.csv")
target = "P10"
labels = labels.pop(target)

all_folds = KFold(n_splits=10, shuffle=True, random_state=1024)

mse = []
mse_train = []
explainvar = []
explainvar_train = []
r2 = []
r2_train = []
pred = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(all_folds.split(pseudoCounts, labels)):
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = labels[train_index]
  ytest = labels[test_index]
  start = time.time()
  clf = MLPRegressor(random_state=2405).fit(xtrain, ytrain)
  timetrain.append(time.time() - start)
  startpred = time.time()
  ypred = clf.predict(xtest)
  ypred_train = clf.predict(xtrain)
  pred.append(ypred)
  mse.append(mean_squared_error(ytest, ypred))
  mse_train.append(mean_squared_error(ytrain, ypred_train))
  explainvar.append(explained_variance_score(ytest, ypred))
  explainvar_train.append(explained_variance_score(ytrain, ypred_train))
  r2.append(r2_score(ytest, ypred))
  r2_train.append(r2_score(ytrain, ypred_train))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_mean_squared_error':mse, 'test_explained_variance':explainvar, 'test_r2':r2, 'train_mean_squared_error': mse_train, 'train_explained_variance': explainvar_train, 'train_r2': r2_train}
filehandler = open("../../results/Simulated/PerceptronScaledScores_P10_2022-08-19.obj", "wb")
pickle.dump(scores, filehandler)

pred = np.ravel(pred)
pd.DataFrame(pred).to_csv("../../results/Simulated/PerceptronScaledPred_P10_2022-08-19.csv", index=False, header=False)

