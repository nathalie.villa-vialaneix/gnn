#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)
# cur_date <- args[1]
cur_date <- "2023-02-09"

# Settings #####
setwd("../..")
library("glmgraph")
library("ROCR")
library("profmem")

# Loading data ####
# loading expression data: samples in rows
expr_file <- "data/Simulated/scaled_expression.csv"
expr <- read.table(expr_file, header = TRUE, sep = ",", row.names = 1)
expr <- as.matrix(expr)

# loading network
graph_file <- "data/Simulated/network.csv"
net_adj <- read.table(graph_file, header = TRUE, sep = ",")
net_adj <- as.matrix(net_adj)
net_adj <- net_adj + t(net_adj) != 0
net_lap <- diag(apply(net_adj, 1, sum)) - net_adj
# removing a node that is not in expr
to_keep <- intersect(colnames(net_adj), colnames(expr))
net_lap <- net_lap[to_keep, to_keep]
rm(net_adj)
gc()

# loading labels
labels <- read.table("data/Simulated/scaled_targets.csv", header = TRUE,
                     sep = ",")
labels <- labels$P10

# loading folds
all_folds <- read.table("data/Simulated/folds.csv", header = TRUE, sep = " ")
all_folds <- unlist(all_folds) + 1
foldsizes <- rep(10, 10)

# CV folds ####
n <- length(labels)

# CV training
out_file <- paste0("glmgraph", "Scores_P10_", cur_date, ".csv")
out_file <- file.path("results/Simulated", out_file)
mem_file <- gsub("\\.csv", "\\.dat", gsub("Scores", "Mem", out_file))

if (!file.exists(out_file)) {
  set.seed(14111455)
  start <- 1
  training_time <- rep(0, 10)
  testing_time <- rep(0, 10)
  mse <- rep(0, 10)
  explained_var <- rep(0, 10)
  r2 <- rep(0, 10)
  mse_train <- rep(0, 10)
  explained_var_train <- rep(0, 10)
  r2_train <- rep(0, 10)
  
  totalmem <- total(profmem({
    for (ind in 1:10) {
      cat("Processing fold...", ind)
      training_time[ind] <- system.time({
        training_obs <- setdiff(1:nrow(expr), 
                                all_folds[start:(start + foldsizes[ind] - 1)])
        start <- start + foldsizes[ind]
        cur_mod <- cv.glmgraph(expr[training_obs, ], labels[training_obs], 
                              net_lap, standardize = FALSE, family = "gaussian", 
                              type.measure = "mse")
      })[3]
      
      testing_time[ind] <- system.time({
        testing_obs <- all_folds[(start - foldsizes[ind]):(start - 1)]
        ytest <- labels[testing_obs]
        ytrain <- labels[training_obs]
        cur_pred <- predict(cur_mod, expr[testing_obs, ])
        cur_pred_train <- predict(cur_mod, expr[training_obs, ])
        mse[ind] <- mean((cur_pred - ytest)^2)
        mse_train[ind] <- mean((cur_pred_train - ytrain)^2)
        explained_var[ind] <- 1 - var(cur_pred - ytest) / var(ytest)
        explained_var_train[ind] <- 1 - var(cur_pred_train - ytrain) / var(ytrain)
        r2[ind] <- 1 - sum((cur_pred - ytest)^2) / sum( (ytest - mean(ytest))^2)
        r2_train[ind] <- 1 - sum((cur_pred_train - ytrain)^2) / sum( (ytrain - mean(ytrain))^2)
        if (is.na(r2[ind])) r2[ind] <- 0
        if (is.na(r2_train[ind])) r2_train[ind] <- 0
      })[3]
    }
  }))
  
  all_res <- cbind(mse, explained_var, r2, mse_train, explained_var_train, 
                   r2_train, training_time, testing_time)
  colnames(all_res) <- c("test_mean_squared_error", "test_explained_variance", 
                         "test_r2", "train_mean_squared_error", 
                         "train_explained_variance", "train_r2", 
                         "training_time", "testing_time")
  
  write.csv(all_res, file = out_file, row.names = FALSE)
  write.table(totalmem, file = mem_file, row.names = FALSE, col.names = FALSE)
}
