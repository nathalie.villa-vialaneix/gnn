#! /bin/bash
mprof -C -o perceptron.dat 3-Perceptron.py
mprof -C -o randomforest.dat 3-RandomForest.py
mprof -o svm.dat 3-SVM.py

R --vanilla -s -f 3-glmgraph.R
R --vanilla -s -f 3-glmgraph_CT.R
R --vanilla -s -f 3-glmgraph_random.R
R --vanilla -s -f 3-glmgraph_complete.R
R --vanilla -s -f 3-RandomForest.R
R --vanilla -s -f 3-SVM.R
python3 4-formatCSV2OBJ.py

mprof -C -o gnn_ppi.dat 3-GNN.py --target P10 --graph network
mprof -C -o gnn_ct.dat 3-GNN.py --target P10 --graph ct_net
mprof -C -o gnn_complete.dat 3-GNN.py --target P10 --graph complete_net
mprof -C -o gnn_random.dat 3-GNN.py --target P10 --graph random_net

mprof -C -o gnnpatient_dnn.dat 3-GNN_patients.py --modelType DNN --scaled_targets True --target P10 --randomGraph 0
mprof -C -o gnnpatient.dat 3-GNN_patients.py --modelType GNN --scaled_targets True --target P10 --randomGraph 0
mprof -C -o gnnpatient_random.dat 3-GNN_patients.py --modelType GNN --scaled_targets True --target P10 --randomGraph 1
