import numpy as np
import spektral
import tensorflow as tf
from tensorflow import keras
from keras.regularizers import l2
from keras.layers import Dense, Dropout, Flatten
from spektral.layers import ChebConv, GATConv, GCNConv, GraphSageConv
import time

def mpool1(x, p):
    """Max pooling of size p (should be a power of 2)."""
    if p > 1:
        x = tf.expand_dims(x, 3)  # N x M x F x 1
        x = tf.nn.max_pool(x, ksize=[1,p,1,1], strides=[1,p,1,1], padding='SAME') #For a given window of p, takes the maximum value within that window
        return tf.squeeze(x, [3])  # N x M/p x F
    else:
        return x
        
def apool1(x, p):
    """Average pooling of size p (should be a power of 2)."""
    if p > 1:
        x = tf.expand_dims(x, 3)  # N x M x F x 1
        x = tf.nn.avg_pool(x, ksize=[1,p,1,1], strides=[1,p,1,1], padding='SAME') #For a given window of p, takes the averaged value within that window
        return tf.squeeze(x, [3])  # N x M/p x F
    else:
        return x
        
def pool1(x, pi, pool_type):
    """General pooling function."""
    if pool_type == 'mpool1':
        xp = mpool1(x, pi)
    if pool_type == 'apool1':
        xp = apool1(x, pi)
        
    return xp

def GNNModel(n_nodes, n_node_features, L, params):
    """
    GNN model based on the same architecture as in Chereda et al. (2021)
    In this model, the number of convolutional/dense layers can vary.
    
    INPUTS:
    n_nodes: number of nodes in the graph
    n_node_features: number of feature per node
    L: list of Graph Laplacians (one per coarsening level)
    params: dict containing the parameters (including the parameters corresponding to the architecture)
    
    OUTPUTs:
    model: Keras model of the GNN
    """

    n_coarse = len(params['p']) # number of convolutional layers
    n_dense = len(params['M']) # number of dense layers
    
    x_in = keras.Input(shape=(n_nodes,n_node_features))
    
    k_init = tf.keras.initializers.TruncatedNormal(0, 0.1, seed=7) # kernel initializer
    b_init = tf.constant_initializer(0.1) # bias initializer
    reg = l2(0.5*params['reg_lambda'])
    
    x = tf.identity(x_in)
    
    for i in range(n_coarse):
        # Convolution layer
        if params['conv'] == "chebnet":
            x = ChebConv(params['F'][i], K=params['K'][i], activation="relu", kernel_initializer=k_init, bias_initializer = b_init)([x, L[i]])
        if params['conv'] == "graphsage":
            x = GraphSageConv(params['F'][i], activation="relu", kernel_initializer=k_init, bias_initializer = b_init)([x, L[i]])
        if params['conv'] == "gat":
            x = GATConv(params['F'][i], activation="relu", kernel_initializer=k_init, bias_initializer = b_init)([x, L[i]])
        if params['conv'] == "gcn":
            x = GCNConv(params['F'][i], activation="relu", kernel_initializer=k_init, bias_initializer = b_init)([x, L[i]])
        
        # Pooling layer
        x = pool1(x, params['p'][i], params['pool1'])
        
    # Flatten
    x = Flatten()(x)
    
    # Dense layers
    for j in range(n_dense - 1):
        x = Dense(params['M'][j], activation=params['activation'], kernel_regularizer=reg, bias_regularizer=reg, kernel_initializer=k_init, bias_initializer=b_init)(x)
        if params['dropout'] < 1: # No dropout if equal to 1
            x = Dropout(1-params['dropout'])(x)
    output = Dense(params['M'][n_dense-1], activation=params['last_activation'], kernel_regularizer=reg, bias_regularizer=reg, kernel_initializer=k_init, bias_initializer=b_init)(x) # softmax on the last layer

    model = keras.models.Model(inputs=x_in, outputs=output)
    
    return model

    
def GNNLearning(data_train, data_test, L, params):
    """
    GNN learning and testing
    
    INPUTS:
    data_train: training data (class BreastCancerDataset)
    data_test: training data (class BreastCancerDataset)
    L: list of Graph Laplacians (one per coarsening level)
    params: dict of parameters
    
    OUTPUTS:
    fit_time: training time
    score_time: test time
    y_test_true : true test labels
    y_test_pred : predicted test labels
    y_train_true : true train labels
    y_train_pred : predicted train labels
    y_test_score : target test scores
    """
    
    n_nodes, n_node_features = data_train[0].x.shape
    n_classes = params['M'][-1] # number of classes

    loader_train = spektral.data.MixedLoader(data_train, batch_size=params['batch_size'], epochs=params['epochs'], shuffle=True)
    loader_test = spektral.data.MixedLoader(data_test, batch_size=params['batch_size'], epochs=1, shuffle=True)

    model = GNNModel(n_nodes, n_node_features, L, params)

    # We use a learning rate schedule that modulates how the learning rate of the optimizer changes over time:
    lr_schedule = keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate = params['learning_rate'],
        decay_steps = params['decay_steps'],
        decay_rate = params['decay_rate'],
        staircase = True)

    if params['momentum'] == 0: # no momentum
        opt = keras.optimizers.Adam(learning_rate = lr_schedule)
    else:
        opt = keras.optimizers.SGD(learning_rate=lr_schedule, momentum=params['momentum'])
    
    loss_fn = keras.losses.SparseCategoricalCrossentropy(from_logits=False)
    model.compile(loss=loss_fn, optimizer=opt)

    # Training model
    @tf.function
    def train_step(inputs, target):
        with tf.GradientTape() as tape:
            predictions = model(inputs[0], training=True)
            loss = loss_fn(target, predictions) + sum(model.losses)
            gradients = tape.gradient(loss, model.trainable_variables)
        opt.apply_gradients(zip(gradients, model.trainable_variables))
        return loss
        
    # Evaluating model
    def evaluate(loader):
        y_true = []
        y_pred = []
        y_score = []
        for batch in loader:
            inputs, target = batch
            pred = model(inputs[0], training=False)
            pred = pred.numpy()
            pred_bin = np.argmax(pred,axis=1)
            n_classes = pred.shape[1]
            if n_classes == 2:
                pred_score = pred[:,1] # uses the scores for the class 1 for computing the auc-roc
            if n_classes > 2:
                pred_score = pred
    
            pred_bin = pred_bin.reshape(pred_bin.size,1)
            if n_classes == 2:
                pred_score = pred_score.reshape(pred_score.size,1)
            target = target.reshape(target.size,1)
    
            y_true.append(target)
            y_pred.append(pred_bin)
            y_score.append(pred_score)

        # Stack the predictions for the different batches
        y_true = np.vstack(y_true)
        y_pred = np.vstack(y_pred)
        y_score = np.vstack(y_score)
        
        return y_true, y_pred, y_score
        
    
    start_train = time.time()
    step = loss = 0
    i = 0
    for batch in loader_train:
        step += 1
        loss += train_step(*batch)
        if step == loader_train.steps_per_epoch:
            step = 0
            i = i+1
            print(f"Epoch {i}: loss = {loss / loader_train.steps_per_epoch}")
            loss = 0
    fit_time = time.time() - start_train

    start_test = time.time()
    y_test_true, y_test_pred, y_test_score = evaluate(loader_test)
    score_time = time.time() - start_test
    
    del loader_train
    loader_val = spektral.data.MixedLoader(data_train, batch_size=params['batch_size'], epochs=1, shuffle=True)
    y_train_true, y_train_pred, y_train_score = evaluate(loader_val)

    return fit_time, score_time, y_test_true, y_test_pred, y_test_score, y_train_true, y_train_pred
    
def GNNReg(data_train, data_test, L, params):
    """
    regression with GNN
    
    INPUTS:
    data_train: training data (class BreastCancerDataset)
    data_test: training data (class BreastCancerDataset)
    L: list of Graph Laplacians (one per coarsening level)
    params: dict of parameters
    
    OUTPUTS:
    fit_time: training time
    score_time: test time
    y_test_true : true test labels
    y_test_pred : predicted test labels
    y_train_true : true training labels
    y_train_pred : predicted training labels
    """
        
    n_nodes, n_node_features = data_train[0].x.shape
    n_classes = params['M'][-1] # number of classes

    loader_train = spektral.data.MixedLoader(data_train, batch_size=params['batch_size'], epochs=params['epochs'], shuffle=True)
    loader_test = spektral.data.MixedLoader(data_test, batch_size=params['batch_size'], epochs=1, shuffle=True)

    model = GNNModel(n_nodes, n_node_features, L, params)

    # We use a learning rate schedule that modulates how the learning rate of the optimizer changes over time:
    lr_schedule = keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate = params['learning_rate'],
        decay_steps = params['decay_steps'],
        decay_rate = params['decay_rate'],
        staircase = True)

    if params['momentum'] == 0: # no momentum
        opt = keras.optimizers.Adam(learning_rate = lr_schedule)
    else:
        opt = keras.optimizers.SGD(learning_rate=lr_schedule, momentum=params['momentum'])
    
    loss_fn = keras.losses.MeanSquaredError()
    model.compile(loss=loss_fn, optimizer=opt)

    # Training model
    @tf.function
    def train_step(inputs, target):
        with tf.GradientTape() as tape:
            predictions = model(inputs[0], training=True)
            loss = loss_fn(target, predictions) + sum(model.losses)
        gradients = tape.gradient(loss, model.trainable_variables)
        opt.apply_gradients(zip(gradients, model.trainable_variables))
        return loss
        
    # Evaluating model
    def evaluate(loader):
        y_true = []
        y_pred = []
        for batch in loader:
            inputs, target = batch
            pred = model(inputs[0], training=False)
            pred = pred.numpy()
            target = target.reshape(target.size,1)
    
            y_true.append(target)
            y_pred.append(pred)

        # Stack the predictions for the different batches
        y_true = np.vstack(y_true)
        y_pred = np.vstack(y_pred)
        
        return y_true, y_pred
    
    start_train = time.time()
    step = loss = 0
    i = 0
    for batch in loader_train:
        step += 1
        loss += train_step(*batch)
        if step == loader_train.steps_per_epoch:
            step = 0
            i = i+1
            print(f"Epoch {i}: loss = {loss / loader_train.steps_per_epoch}")
            loss = 0
    fit_time = time.time() - start_train

    # Evaluate model
    start_test = time.time()
    y_test_true, y_test_pred = evaluate(loader_test)
    score_time = time.time() - start_test
    
    del loader_train
    loader_val = spektral.data.MixedLoader(data_train, batch_size=params['batch_size'], epochs=1, shuffle=True)
    y_train_true, y_train_pred = evaluate(loader_val)


    return fit_time, score_time, y_test_true, y_test_pred, y_train_true, y_train_pred


