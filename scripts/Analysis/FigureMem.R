library("dplyr")
library("RColorBrewer")
library("ggplot2")


## Useful function
make_mem_barplot <- function(input_files, methods, dataset, postname, 
                             networks = NULL, implementations = NULL, 
                             convolution = NULL, regression = FALSE, 
                             use_R = FALSE) {
  
  ref_methods <- c("GNN", "MLP", "RF", "SVM", "GNNo", "glmgraph")
  my_palette <- brewer.pal(9, "Set1")
  my_palette <- my_palette[ref_methods %in% methods]
  ref_methods <- ref_methods[ref_methods %in% methods]
  
  if (!is.null(networks)) {
    ref_networks <- c("original", "Cor", "random", "complete", "NC")
    my_shapes <- 15:19
    my_shapes <- my_shapes[ref_networks %in% networks]
    ref_networks <- ref_networks[ref_networks %in% networks]
    ref_names <- paste(rep(ref_methods, each = length(ref_networks)),
                       rep(ref_networks, length(ref_methods)),
                       sep = "+")
    obs_names <- paste(methods, networks, sep = "+")
    ref_names <- ref_names[ref_names %in% obs_names]
    ref_names <- gsub("\\+NC", "", ref_names)
  }
  if (!is.null(implementations)) {
    ref_implementation <- c("original", "keras", "R", "Chereda", "no tuning")
    my_shapes <- 15:19
    my_shapes <- my_shapes[ref_implementation %in% implementations]
    ref_implementation <- ref_implementation[ref_implementation %in% implementations]
    ref_names <- paste(rep(ref_methods, each = length(ref_implementation)),
                       rep(ref_implementation, length(ref_methods)),
                       sep = "+")
    obs_names <- paste(methods, implementations, sep = "+")
    ref_names <- ref_names[ref_names %in% obs_names]
  }
  if (!is.null(convolution)) {
    ref_convolution <- c("original", "Sage", "GCN")
    my_shapes <- 15:17
    my_shapes <- my_shapes[ref_convolution %in% convolution]
    ref_convolution <- ref_convolution[ref_convolution %in% convolution]
    ref_names <- paste(rep(ref_methods, each = length(ref_convolution)),
                       rep(ref_convolution, length(ref_methods)),
                       sep = "+")
    obs_names <- paste(methods, convolution, sep = "+")
    ref_names <- ref_names[ref_names %in% obs_names]
  }
  
  input_files <- paste0(input_files, ".dat")
  input_files <- file.path("../../results", dataset, input_files)
  
  if (use_R) {
    all_data <- unlist(sapply(input_files, read.table)) / 10^6
  } else {
    all_data <- lapply(input_files, read.table, sep = " ", comment.char = "C")
    all_data <- lapply(all_data, "[[", 2)
    all_data <- sapply(all_data, max)
  }
  
  if (!is.null(networks)) {
    all_data <- data.frame("memory" = all_data, "method" = methods,
                           "network" = networks)
    all_data$network <- factor(all_data$network, levels = ref_networks, 
                               ordered = TRUE)
  } else if (!is.null(implementations)) {
    all_data <- data.frame("memory" = all_data, "method" = methods,
                           "implementation" = implementations)
    all_data$implementation <- factor(all_data$implementation, 
                                      levels = ref_implementation, 
                                      ordered = TRUE)
  } else if (!is.null(convolution)) {
    all_data <- data.frame("memory" = all_data, "method" = methods,
                           "convolution" = convolution)
    all_data$convolution <- factor(all_data$convolution, 
                                   levels = ref_convolution, 
                                   ordered = TRUE)
  } else {
    all_data <- data.frame("memory" = all_data, "method" = methods)
  }
  all_data$method <- factor(all_data$method, levels = ref_methods, 
                            ordered = TRUE)
  if (!is.null(networks)) {
    all_data$name <- paste(as.character(all_data$method), 
                           as.character(all_data$network), sep = "+")
    all_data$name <- gsub("\\+NC", "", all_data$name)
    all_data$name <- factor(all_data$name, levels = ref_names, ordered = TRUE)
  } else if (!is.null(implementations)) {
    all_data$name <- paste(as.character(all_data$method), 
                           as.character(all_data$implementation), sep = "+")
    all_data$name <- factor(all_data$name, levels = ref_names, ordered = TRUE)
  } else if (!is.null(convolution)) {
    all_data$name <- paste(as.character(all_data$method), 
                           as.character(all_data$convolution), sep = "+")
    all_data$name <- factor(all_data$name, levels = ref_names, ordered = TRUE)
  } else {
    all_data$name <- all_data$method
  }
  
  if (use_R) {
    ylabel <- "Memory (MiB; total)"
  } else ylabel <- "Memory (MiB; max)"
  
  p <- ggplot(all_data, aes(x = name, y = memory, fill = method)) +
    geom_bar(stat = "identity") + theme_bw() + ylab(ylabel) + 
    theme(axis.title.x = element_blank(), legend.position = "none") +
    scale_fill_manual(values = my_palette)
  
  if (grepl("graph", postname) | grepl("implementation", postname) | grepl("convolution", postname)) {
    p <- p + theme(axis.text.x = element_text(angle = 55, vjust = 0.5))
  }
  
  out_name <- paste("barplot", dataset, "memory", postname, sep = "-")
  out_name <- paste0(out_name, ".png")
  out_name <- file.path("../../results/Figures", out_name)
  widthp <- max(8 / 5 * length(methods), 3.2)
  if (is.null(networks) & is.null(implementations) & is.null(implementations)) {
    ggsave(out_name, plot = p, width = widthp, height = 6, units = "cm")
  } else {
    ggsave(out_name, plot = p, width = widthp, height = 7, units = "cm")
  }
  return(p)
}

# Base scenario ####

## BreastCancer / not scaled data ####
methods <- c("GNN", "MLP", "RF", "SVM", "GNNo")
input_files <- c("mprof_gnn_ppi", "mprof_perceptron", "mprof_randomforest", 
                 "mprof_svm", "mprof_gnnpatient")

make_mem_barplot(input_files, methods, "BreastCancer", "base_not_scaled")


## CancerType / PPI + singleton ####
input_files <- c("mprof_gnn_chereda_PPIA", "mprof_perceptron_ppi",
                 "mprof_randomforest_ppi", "mprof_svm_ppi",
                 "mprof_gnnpatient_PPI")
methods <- c("GNN","MLP","RF","SVM","GNNo")

make_mem_barplot(input_files, methods, "CancerType", postname = "basePPIA")


## F1000 / MOA ####
input_files <- c("gnn_full_moa", "perceptron_moa", "randomforest_moa", 
                 "svm_moa", "gnnpatient_moa")
methods <- c("GNN", "MLP", "RF", "SVM", "GNNo")

make_mem_barplot(input_files, methods, "F1000", postname = "base")


## F1000 / Subtype (full) ####
input_files <- c("gnn_fullsubtype", "perceptron_fullsubtype", 
                 "randomforest_fullsubtype")
methods <- c("GNN", "MLP", "RF")

make_mem_barplot(input_files, methods, "F1000", postname = "baseST")


## F1000 / Primarysite (full) ####
input_files <- c("gnn_fullprimarysite", "perceptron_primarysite", 
                 "randomforest_fullprimarysite")
methods <- c("GNN", "MLP", "RF")

make_mem_barplot(input_files, methods, "F1000", postname = "basePS")


## F1000 / MOA (full) ####
input_files <- c("gnn_fullmoa", "perceptron_fullmoa", 
                 "randomforest_fullmoa")
methods <- c("GNN", "MLP", "RF")

make_mem_barplot(input_files, methods, "F1000", postname = "baseFM")


## Simulated ####
methods <- c("GNN", "MLP", "RF", "SVM", "GNNo")
input_files <- c("gnn_ppi", "perceptron", "randomforest", "svm", "gnnpatient")

make_mem_barplot(input_files, methods, "Simulated", postname = "base")


## DREAM5 / scaled data ####
methods <- c("GNN", "MLP", "RF", "SVM", "GNNo")
input_files <- c("gnn_scaled", "perceptron_scaled", "randomforest_scaled", 
                 "svm_scaled", "gnnpatient_scaled")

make_mem_barplot(input_files, methods, "DREAM5", postname = "base")


# Input network comparison ####

## BreastCancer / not scaled data ####
input_files <- c("mprof_gnn_ppi", "mprof_gnn_ct", "mprof_gnn_random", 
                 "mprof_gnn_complete", "mprof_gnnpatient", 
                 "mprof_gnnpatient_random")
methods <- c("GNN", "GNN", "GNN", "GNN", "GNNo", "GNNo")
networks <- c("original", "Cor", "random", "complete", "original", "random")

make_mem_barplot(input_files, methods, "BreastCancer", "graphs", networks)


## Simulated ####
input_files <- c("gnn_ppi", "gnn_ct", "gnn_random", "gnn_complete", 
                 "gnnpatient", "gnnpatient_random")
methods <- c("GNN", "GNN", "GNN", "GNN", "GNNo", "GNNo")
networks <- c("original", "Cor", "random", "complete", "original", "random")

## explained variance
make_mem_barplot(input_files, methods, "Simulated", "graph", networks)


# Implementation comparison ####

## BreastCancer / not scaled data ####
input_files <- c("mprof_perceptron", "mprof_gnnpatient_dnn")
methods <- c("MLP", "MLP")
implementations <- c("original", "keras")

## memory usage
make_mem_barplot(input_files, methods, "BreastCancer", 
                 "implementation_not_scaled", implementations = implementations)


## CancerType / PPI + singleton ####
input_files <- c("mprof_gnn_chereda_PPIA", "mprof_gnn_ramirez_PPIA",
                 "mprof_perceptron_ppi", "mprof_gnnpatient_PPI_dnn")
methods <- c("GNN","GNN","MLP","MLP")
implementations <- c("original", "Chereda", "original", "keras")

make_mem_barplot(input_files, methods, "CancerType", 
                 postname = "implementation_PPIA", 
                 implementations = implementations)


## F1000 / MOA ####
input_files <- c("perceptron_moa", "perceptronalt_moa", "gnnpatient_dnn_moa",
                 "randomforest_moa", "randomforestalt_moa")
methods <- c("MLP", "MLP", "MLP", "RF", "RF")
implementations <- c("original", "no tuning" , "keras", "original", "no tuning")

make_mem_barplot(input_files, methods, "F1000", postname = "implementation", 
                 implementations = implementations)


## F1000 / Subtype (full) ####
input_files <- c("perceptron_fullsubtype", "perceptronalt_fullsubtype", 
                 "gnnpatient_dnn_fullsubtype", "randomforest_fullsubtype",
                 "randomforestalt_fullsubtype")
methods <- c("MLP", "MLP", "MLP", "RF", "RF")
implementations <- c("original", "no tuning" , "keras", "original", "no tuning")

make_mem_barplot(input_files, methods, "F1000", postname = "implementationST", 
                 implementations = implementations)


## F1000 / Primarysite (full) ####
input_files <- c("perceptron_primarysite", "perceptronalt_primarysite", 
                 "gnnpatient_dnn_fullprimarysite", 
                 "randomforest_fullprimarysite", 
                 "randomforestalt_fullprimarysite")
methods <- c("MLP", "MLP", "MLP", "RF", "RF")
implementations <- c("original", "no tuning" , "keras", "original", "no tuning")

make_mem_barplot(input_files, methods, "F1000", postname = "implementationPS", 
                 implementations = implementations)


## F1000 / MOA (full) ####
input_files <- c("perceptron_fullmoa", "perceptronalt_fullmoa", 
                 "gnnpatient_dnn_fullmoa", "randomforest_fullmoa", 
                 "randomforestalt_fullmoa")
methods <- c("MLP", "MLP", "MLP", "RF", "RF")
implementations <- c("original", "no tuning" , "keras", "original", "no tuning")

make_mem_barplot(input_files, methods, "F1000", postname = "implementationFM", 
                 implementations = implementations)


## Simulated ####
input_files <- c("perceptron", "gnnpatient_dnn")
methods <- c("MLP", "MLP")
implementations <- c("original", "keras")

make_mem_barplot(input_files, methods, "Simulated", "implementation",
                 implementations = implementations)


## DREAM5 / scaled data ####
input_files <- c("perceptron_scaled", "gnnpatient_dnn_scaled")
methods <- c("MLP", "MLP")
implementations <- c("original", "keras")

make_mem_barplot(input_files, methods, "DREAM5", "implementation",
                 implementations = implementations)


# Convolutional layer comparison ####

## BreastCancer / not scaled data ####
input_files <- c("mprof_gnn_ppi", "mprof_gnn_ppi_graphsage", 
                 "mprof_gnn_ppi_gcn")
methods <- c("GNN", "GNN", "GNN")
convolution <- c("original", "Sage", "GCN")

make_mem_barplot(input_files, methods, "BreastCancer", "convolution", 
                 convolution = convolution)

# R scripts ####

## BreastCancer / not scaled data ####
input_files <- c("glmgraphHPRD_PPI_2023-11-09", "glmgraphHPRD_ct_2023-11-09",
                 "glmgraphHPRD_random_2023-11-09", 
                 "RRandomForestMem_2023-11-09", "RSVMMem_2023-11-09")
methods <- c("glmgraph", "glmgraph", "glmgraph", "RF", "SVM")
networks <- c("original", "Cor", "random", "NC", "NC")

make_mem_barplot(input_files, methods, "BreastCancer", "Rscripts-graphs", 
                 networks, use_R = TRUE)


## CancerType / PPI + singleton ####
input_files <- c("RRandomForest_PPI_Mem_2023-11-09", "RSVM_PPI_Mem_2023-11-09")
methods <- c("RF", "SVM")

make_mem_barplot(input_files, methods, "CancerType", "Rscripts", use_R = TRUE)


## F1000 / MOA ####
input_files <- c("RRandomForest_Mem_2023-11-09", "RSVM_Mem_2023-11-09")
methods <- c("RF", "SVM")

make_mem_barplot(input_files, methods, "F1000", "Rscripts", use_R = TRUE)


## F1000 / Subtype (full) ####
input_files <- c("RRandomForest_full_subtype_Mem_2023-11-09")
methods <- "RF"

make_mem_barplot(input_files, methods, "F1000", "RscriptsST", use_R = TRUE)


## F1000 / Primarysite (full) ####
input_files <- c("RRandomForest_full_primarysite_Mem_2023-11-09")
methods <- "RF"

make_mem_barplot(input_files, methods, "F1000", "RscriptsPS", use_R = TRUE)


## F1000 / MOA (full) ####
input_files <- c("RRandomForest_full_moa_Mem_2023-11-09")
methods <- "RF"

make_mem_barplot(input_files, methods, "F1000", "RscriptsFM", use_R = TRUE)


## Simulated ####
input_files <- c("glmgraphMem_P10_2023-11-09", "glmgraph_CTMem_P10_2023-11-09",
                 "glmgraph_randomMem_P10_2023-11-09", 
                 "glmgraph_completeMem_P10_2023-11-09",
                 "RRandomForest_Mem_P10_2023-11-09", "RSVM_Mem_P10_2023-11-09")
methods <- c("glmgraph", "glmgraph", "glmgraph", "glmgraph", "RF", "SVM")
networks <- c("original", "Cor", "random", "complete", "NC", "NC")

make_mem_barplot(input_files, methods, "Simulated", "Rscripts-graphs", 
                 networks, use_R = TRUE)


## DREAM5 / scaled data ####
input_files <- c("glmgraph_scaledMem_2023-11-09", 
                 "RRandomForest_scaled_Mem_2023-11-09", 
                 "RSVM_scaled_Mem_2023-11-09")
methods <- c("glmgraph", "RF", "SVM")

make_mem_barplot(input_files, methods, "DREAM5", "Rscripts-graphs", 
                 use_R = TRUE)
