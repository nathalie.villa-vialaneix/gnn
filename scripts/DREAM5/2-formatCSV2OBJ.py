import pandas as pd
import pickle
import numpy as np

results = pd.read_csv("../../results/DREAM5/glmgraph_scaledScores_2023-11-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/DREAM5/glmgraph_scaledScores_2023-11-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/DREAM5/RRandomForest_scaled_Scores_2023-11-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/DREAM5/RRandomForest_scaled_Scores_2023-11-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/DREAM5/RSVM_scaled_Scores_2023-11-09.csv")
formatr = {"fit_time": results.transpose().to_numpy()[6], "score_time": results.transpose().to_numpy()[7], "test_mean_squared_error": results.transpose().to_numpy()[0], "test_explained_variance": results.transpose().to_numpy()[1], "test_r2": results.transpose().to_numpy()[2], "train_mean_squared_error": results.transpose().to_numpy()[3], "train_explained_variance": results.transpose().to_numpy()[4], "train_r2": results.transpose().to_numpy()[5]}
filehandler = open("../../results/DREAM5/RSVM_scaled_Scores_2023-11-09.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()
