import sys
sys.path.append('../GNN_spektral')
import os
import numpy as np
import tensorflow as tf
import spektral
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score
from graph_lrp_master.lib import coarsening, graph
from models import GNNReg
import pickle


# Parameters

params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 109
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 1 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['decay_rate'] = 0.95 # Base of exponential decay. No decay with 1.
params['reg_lambda'] = 1e-4 # regularization parameter
params['momentum'] = 0 # 0 indicates no momentum
params['activation'] = "relu"
params['last_activation'] = "linear"
params['conv'] = "chebnet" # convolutional layer

# Parameters of the GNN architecture
params['F'] = [32, 32] # Number of graph convolutional filters
params['K'] = [8, 8] # Polynomial orders
params['p'] = [2, 2] # Pooling sizes
params['M'] = [512, 128, 1] # Output dimensionality of fully connected layers
params['pool1'] = 'mpool1'

X = pd.read_csv("../../data/DREAM5/expr_predictors_scaled.tab", delimiter="\t")
X = X.values.astype(np.float64)
y = pd.read_csv("../../data/DREAM5/target_scaled.tab", header=None)
y = np.ravel(y)


A = pd.read_csv("../../data/DREAM5/adjacency_matrix.tab", delimiter="\t")
A = csr_matrix(A.values.astype(np.float64).astype(np.float32))

# Coarsening the graph
graphs, perm = coarsening.coarsen(A, levels=2, self_connections=False)
X = coarsening.perm_data(X, perm) # rearrange the vertices in order to have a binary tree structure
L = [spektral.layers.ChebConv.preprocess(A) for A in graphs] # normalized Laplacian
L = [spektral.utils.sparse.sp_matrix_to_sp_tensor(A) for A in L] # convert to sparse tensor

del graphs
del A

# Creating a custom Spektral dataset
class BreastCancerDataset(spektral.data.dataset.Dataset):
    def read(self):
        self.a = L[0]
        return [spektral.data.graph.Graph(x=X[i].reshape(X[i].size,1), y=y[i]) for i in range(X.shape[0])]
        
data = BreastCancerDataset()

# Cross-validation
n_folds = 10
mse_train = np.zeros(n_folds)
mse_test = np.zeros(n_folds)
r2_train = np.zeros(n_folds)
r2_test = np.zeros(n_folds)
explainvar_train = np.zeros(n_folds)
explainvar_test = np.zeros(n_folds)
fit_time = np.zeros(n_folds)
score_time = np.zeros(n_folds)
skf = KFold(n_splits=10, shuffle=True, random_state=1514)
for fold, (train_index, test_index) in enumerate(skf.split(X, y)):
    print(f"Fold = {fold}")

    data_train = data[train_index]
    data_test = data[test_index]
    
    params['decay_steps'] = len(data_train) / params['batch_size']
    
    # Training GNN and Testing
    fit_time[fold], score_time[fold], y_test_true, y_test_pred, y_train_true, y_train_pred = GNNReg(data_train, data_test, L, params)
    
    mse_train[fold] = mean_squared_error(y_train_true, y_train_pred)
    mse_test[fold] = mean_squared_error(y_test_true, y_test_pred)
    r2_train[fold] = r2_score(y_train_true, y_train_pred)
    r2_test[fold] = r2_score(y_test_true, y_test_pred)
    explainvar_train[fold] = explained_variance_score(y_train_true, y_train_pred)
    explainvar_test[fold] = explained_variance_score(y_test_true, y_test_pred)

    print(f"Train:  Mse = {mse_train[fold]:.3f} ; Explained var = {explainvar_train[fold]:.3f} ; R2 = {r2_train[fold]:.3f}")
    print(f"Test:  Mse = {mse_test[fold]:.3f} ; Explained var = {explainvar_test[fold]:.3f} ; R2 = {r2_test[fold]:.3f}")

score = {'fit_time':fit_time,'score_time':score_time,'test_mean_squared_error':mse_test,'test_r2':r2_test,'test_explained_variance':explainvar_test,'train_mean_squared_error':mse_train,'train_r2':r2_train,'train_explained_variance':explainvar_train}

# Save results
results_filename ='../../results/DREAM5/GNN_scaledScores_2023-11-09.obj'
with open(results_filename, 'wb') as handle:
    pickle.dump(score, handle)
