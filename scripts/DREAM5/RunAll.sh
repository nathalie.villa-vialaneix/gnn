#! /bin/bash
mprof run -C -o perceptron_scaled.dat 1-Perceptron_scaled.py
mprof run -C -T 1 -o randomforest_scaled.dat 1-RandomForest_scaled.py
mprof run -o svm_scaled.dat 1-SVM_scaled.py

R --vanilla -s -f 1-glmgraph_scaled.R
R --vanilla -s -f 1-RandomForest_scaled.R
R --vanilla -s -f 1-SVM_scaled.R
python3 2-formatCSV2OBJ.py

mprof run -C -T 1 -o gnn_scaled.dat 1-GNN_scaled.py
mprof run -C -o gnnpatient_dnn_scaled.dat 1-GNN_patients_scaled.py --modelType DNN --graph 0
mprof run -C -o gnnpatient_scaled.dat 1-GNN_patients_scaled.py --modelType GNN --graph 0
