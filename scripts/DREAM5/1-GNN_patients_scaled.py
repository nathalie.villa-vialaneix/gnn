#!/usr/bin/env python
# coding: utf-8


# Training of graph neural network (GNN) whose convolution is done over the patients.
# Training of dense neural network (DNN) is also included.

import os
os.getcwd()


import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score
import pickle
import time
import argparse

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping
from scipy import sparse
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.model_selection import KFold

import sys
sys.path.append('../GNN_keras')
from models_gnn_patients import *

#tf.keras.utils.set_random_seed(seed=100)
tf.random.set_seed(seed=100)

# Parameters
parser = argparse.ArgumentParser(description='GNN applied to cancer type dataset.')
parser.add_argument('--modelType', type=str, help='GNN or DNN')
parser.add_argument('--graph', type=int, help='indicates which graph is used (1: fully connected; 0: graph estimated from data).')

# Get the arguments from the command line
args = parser.parse_args()
modelType=args.modelType
randomGraph=args.graph
print(modelType)
print(randomGraph)

## 1. with PPI data ###
## Load expression data
pseudoCounts = pd.read_csv("../../data/DREAM5/expr_predictors_scaled.tab", delimiter="\t")
pseudoCounts = pseudoCounts.values.astype(np.float64)
#from sklearn.preprocessing import StandardScaler
#std_scaler = StandardScaler()
#pseudoCounts = std_scaler.fit_transform(pseudoCounts)

### Load target
Y = pd.read_csv("../../data/DREAM5/target_scaled.tab", header=None)
Y = np.ravel(Y)
num_classes=1


# Make similarity graph between patients
if randomGraph == 0:
    A=euclidean_distances(pseudoCounts)
    A=A/np.max(A)
    A=1-A
    np.fill_diagonal(A,0)
    A[A<np.quantile(A,0.998)]=0
    A[A>=np.quantile(A,0.998)]=1
    file_graph = "../../data/DREAM5/graph_patient_PPI.csv" 
    pd.DataFrame(A).to_csv(file_graph, header=False, index=False)

elif randomGraph == 1:
    A = np.ones((pseudoCounts.shape[0],pseudoCounts.shape[0]))


# Make data for the graph convolutional network
edges=sparse.csr_matrix(A)
edges=np.stack((edges.nonzero()[0],edges.nonzero()[1]),axis=0)
#print(edges[:,0:10])
# Create an edge weights array of ones.
edge_weights = tf.ones(shape=edges.shape[1])
# Create a node features array of shape [num_nodes, num_features].
node_features = tf.cast(pseudoCounts, dtype=tf.dtypes.float32)
# Create graph info tuple with node_features, edges, and edge_weights.
graph_info = (node_features, edges, edge_weights)

print("Edges shape:", edges.shape)
print("Nodes shape:", node_features.shape)


# Parameters
params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 200
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 0 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['reg_lambda'] = 1e-4 # regularization parameter
params['F'] = 100 # Number of graph convolutional filters
params['num_classes'] = num_classes
params['activation'] = "relu"
params['last_activation'] = "softmax"



### Train
all_folds = KFold(n_splits=10, shuffle=True, random_state=1514)
n_folds=5
mse = []
mse_train = []
explainvar = []
explainvar_train = []
r2 = []
r2_train = []
pred = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(all_folds.split(pseudoCounts, Y)):
  import gc
  gc.collect()
    
  xtrain = pseudoCounts[train_index,:]
  xtest = pseudoCounts[test_index,:]
  ytrain = Y[train_index]
  ytest = Y[test_index]
  start = time.time()
  if modelType=="GNN":
        model = run_GNN_patient(train_index, ytrain, graph_info, params)
  elif modelType=="DNN":
        model = run_DNN(xtrain, ytrain, params)
  timetrain.append(time.time() - start)
  startpred = time.time()
  if modelType=="GNN":
        ypred=model.predict(x=np.array(test_index), verbose=0)
        train_ypred=model.predict(x=np.array(train_index), verbose=0)
  elif modelType=="DNN":  
        ypred=model.predict(x=xtest, verbose=0)
        train_ypred=model.predict(x=xtrain, verbose=0)           
  
  pred.append(ypred)
  mse.append(mean_squared_error(ytest, ypred))
  mse_train.append(mean_squared_error(ytrain, train_ypred))
  explainvar.append(explained_variance_score(ytest, ypred))
  explainvar_train.append(explained_variance_score(ytrain, train_ypred))
  r2.append(r2_score(ytest, ypred))
  r2_train.append(r2_score(ytrain, train_ypred))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_mean_squared_error':mse, 'test_explained_variance':explainvar, 'test_r2':r2, 'train_mean_squared_error': mse_train, 'train_explained_variance': explainvar_train, 'train_r2': r2_train}


### Export
filehandler = open("../../results/DREAM5/"+modelType+"_patient_scaledScores_2023-11-09.obj", "wb") 
pickle.dump(scores, filehandler)
filehandler.close()



