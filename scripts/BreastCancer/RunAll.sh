#! /bin/bash
mprof run -C -o mprof_perceptron.dat 2-Perceptron.py
mprof run -C -o mprof_perceptron_scaled.dat 2-Perceptron_scaled.py
mprof run -C -o mprof_randomforest.dat 2-RandomForest.py
mprof run -o mprof_svm.dat 2-SVM.py
mprof run -o mprof_svm_scaled.dat 2-SVM_scaled.py

# Remark: as the glmgraph scripts can take several days to run, it is recommanded to run them on different cpus at the same time rather than one after another.
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI_scaled.csv HPRD_complete.csv none 2023-02-09
# Rscript --vanilla 2-glmgraph.R GEO_HG_PPI.csv HPRD_complete.csv none 2023-02-09 too slow
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI_scaled.csv HPRD_PPI.csv comma 2023-02-09
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI.csv HPRD_PPI.csv comma 2023-02-09
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI_scaled.csv HPRD_random.csv none 2023-02-09
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI.csv HPRD_random.csv none 2023-02-09
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI_scaled.csv HPRD_ct.csv none 2023-02-09
Rscript --vanilla 2-glmgraph.R GEO_HG_PPI.csv HPRD_ct.csv none 2023-02-09

R --vanilla -s -f 2-RandomForest.R
R --vanilla -s -f 2-SVM.R
R --vanilla -s -f 2-SVM_scaled.R
python3 3-formatCSV2OBJ.py

mprof run -C -o mprof_gnnpatient_dnn.dat 2-GNN_patients.py --modelType DNN --randomGraph 0 --scaled 0
mprof run -C -o mprof_gnnpatient_dnn_scaled.dat 2-GNN_patients.py --modelType DNN --randomGraph 0 --scaled 1
mprof run -C -o mprof_gnnpatient.dat 2-GNN_patients.py --modelType GNN --randomGraph 0 --scaled 0
mprof run -C -o mprof_gnnpatient_scaled.dat 2-GNN_patients.py --modelType GNN --randomGraph 0 --scaled 1
mprof run -C -o mprof_gnnpatient_random.dat 2-GNN_patients.py --modelType GNN --randomGraph 1 --scaled 0
mprof run -C -o mprof_gnnpatient_random_scaled.dat 2-GNN_patients.py --modelType GNN --randomGraph 1 --scaled 1

mprof run -C -o mprof_gnn_ppi.dat 2-GNN.py --graph HPRD_PPI --scaled 0
mprof run -C -o mprof_gnn_ppi_scaled.dat 2-GNN.py --graph HPRD_PPI --scaled 1
mprof run -C -o mprof_gnn_ct.dat 2-GNN.py --graph HPRD_ct --scaled 0
mprof run -C -o mprof_gnn_ct_scaled.dat 2-GNN.py --graph HPRD_ct --scaled 1
mprof run -C -o mprof_gnn_complete.dat 2-GNN.py --graph HPRD_complete --scaled 0
mprof run -C -o mprof_gnn_complete_scaled.dat 2-GNN.py --graph HPRD_complete --scaled 1
mprof run -C -o mprof_gnn_random.dat 2-GNN.py --graph HPRD_random --scaled 0
mprof run -C -o mprof_gnn_random_scaled.dat 2-GNN.py --graph HPRD_random --scaled 1

mprof run -C -o mprof_gnn_ppi_gcn_scaled.dat 2-GNN_conv.py --graph HPRD_PPI --scaled 1 --conv gcn
mprof run -C -o mprof_gnn_ppi_gcn.dat 2-GNN_conv.py --graph HPRD_PPI --scaled 0 --conv gcn
mprof run -C -o mprof_gnn_ppi_graphsage_scaled.dat 2-GNN_conv.py --graph HPRD_PPI --scaled 1 --conv graphsage
mprof run -C -o mprof_gnn_ppi_graphsage.dat 2-GNN_conv.py --graph HPRD_PPI --scaled 0 --conv graphsage
