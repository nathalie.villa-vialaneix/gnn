import pandas as pd
import pickle
import numpy as np

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_complete_scaled_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_completeScores_scaled_2022-02-17.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_ct_scaled_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_ctScores_scaled_2022-02-17.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_PPI_scaled_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_PPIScores_scaled_2022-02-17.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_random_scaled_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_randomScores_scaled_2022-02-17.obj", "wb")
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_ct_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_ctScores_2022-02-17.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_PPI_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_PPIScores_2022-02-17.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/BreastCancer/glmgraphHPRD_random_2022-02-17.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[2], "train_accuracy": results.transpose().to_numpy()[3], "train_balanced_accuracy": results.transpose().to_numpy()[4]}
filehandler = open("../../results/BreastCancer/glmgraphHPRD_randomScores_2022-02-17.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/BreastCancer/RSVMScores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/BreastCancer/RSVMScores_2023-01-26.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/BreastCancer/RSVMScores_scaled_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/BreastCancer/RSVMScores_scaled_2023-01-26.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

results = pd.read_csv("../../results/BreastCancer/RRandomForestScores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/BreastCancer/RRandomForestScores_2023-01-26.obj", "wb")
pickle.dump(formatr, filehandler)
filehandler.close()

## notes
### unscaled for complete can not be computed

