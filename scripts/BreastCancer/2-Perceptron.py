import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_validate
import pickle
import time

# A. Basic perceptron ###
## 1. with unscaled data (minimum per row removed) ###
pseudoCounts = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI.csv")
pseudoCounts.index = list(pseudoCounts.pop("probe"))
pseudoCounts = pseudoCounts.transpose()
pseudoCounts = pseudoCounts.values.astype(np.float64)
pseudoCounts = pseudoCounts - np.min(pseudoCounts) # Making data lying in the interval [0, 8.35]

labels = pd.read_csv("../../data/BreastCancer/labels_GEO_HG.csv")
labels = labels.transpose().to_numpy().ravel()

PC = MLPClassifier(random_state=1647) ## Note: no regularization or early stopping... might be improved here
all_folds = StratifiedKFold(n_splits=10, shuffle=True, random_state=811)
scores = cross_validate(PC, X=pseudoCounts, y=labels, cv=all_folds, n_jobs=1, scoring=("accuracy", "balanced_accuracy", "roc_auc"), return_train_score=True)

filehandler = open("../../results/BreastCancer/PerceptronScores_2021-11-25.obj", "wb")
pickle.dump(scores, filehandler)



