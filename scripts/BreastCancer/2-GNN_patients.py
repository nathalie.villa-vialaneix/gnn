#!/usr/bin/env python
# coding: utf-8


# Training of graph neural network (GNN) whose convolution is done over the patients.
# Training of dense neural network (DNN) is also included.

import os
os.getcwd()

import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, roc_curve, auc, f1_score, balanced_accuracy_score, roc_auc_score
import pickle
import time
import argparse

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from tensorflow.keras.callbacks import EarlyStopping
from scipy import sparse
from sklearn.metrics.pairwise import euclidean_distances

import sys
sys.path.append('../GNN_keras')
from models_gnn_patients import *

#tf.keras.utils.set_random_seed(seed=100)
tf.random.set_seed(seed=100)


# Parameters
parser = argparse.ArgumentParser(description='GNN applied to breast cancer dataset.')
parser.add_argument('--modelType', type=str, help='GNN or DNN')
parser.add_argument('--scaled', type=int, help='indicates if scaling is used on features (1: scaling; 0: no scaling).')
parser.add_argument('--randomGraph', type=int, help='indicates if random graph is used (2: complete graph; 1: random graph; 0: graph estimated from data).')

# Get the arguments from the command line
args = parser.parse_args()
modelType=args.modelType
scaled=args.scaled
randomGraph=args.randomGraph
print(modelType)
print(scaled)
print(randomGraph)

# Loading data with unscaled data
if scaled == 0:
    X = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI.csv")
    cols = X.columns.tolist()
    X = X[cols[:-1]]
    X = X.transpose()
    X = X.values.astype(np.float64)
    X = X - np.min(X) # Making data lying in the interval [0, 8.35]

if scaled == 1:
    X = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI_scaled.csv",index_col=0)
    X = X.values.astype(np.float64)

y = pd.read_csv("../../data/BreastCancer/labels_GEO_HG.csv")
y = y.values[0].astype(int)
num_classes = len(np.unique(y))


# Make similarity graph between patients
if modelType == "GNN":
    if randomGraph == 0:
        A=euclidean_distances(X)
        A=A/np.max(A)
        A=1-A
        np.fill_diagonal(A,0)
        A[A<np.quantile(A,0.998)]=0
        A[A>=np.quantile(A,0.998)]=1
        if scaled == 0:
            file_graph = "../../data/BreastCancer/graph_patientScores.csv"
        else:
            file_graph = "../../data/BreastCancer/graph_patientScores_scaled.csv"
        pd.DataFrame(A).to_csv(file_graph, header=False, index=False)

    if randomGraph == 1:
        if scaled == 0:
            file_graph = "../../data/BreastCancer/graph_patientScores_random.csv"
        else:
            file_graph = "../../data/BreastCancer/graph_patientScores_scaled_random.csv"
    
        A = pd.read_csv(file_graph,header=None)

    if randomGraph == 2:
        A = np.ones((X.shape[0],X.shape[0]))


    # Make data for the graph convolutional network
    edges=sparse.csr_matrix(A)
    edges=np.stack((edges.nonzero()[0],edges.nonzero()[1]),axis=0)
    print(edges[:,0:10])
    # Create an edge weights array of ones.
    edge_weights = tf.ones(shape=edges.shape[1])
    # Create a node features array of shape [num_nodes, num_features].
    node_features = tf.cast(X, dtype=tf.dtypes.float32)
    # Create graph info tuple with node_features, edges, and edge_weights.
    graph_info = (node_features, edges, edge_weights)

    print("Edges shape:", edges.shape)
    print("Nodes shape:", node_features.shape)


# Parameters
params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 200
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 0 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['reg_lambda'] = 1e-4 # regularization parameter
params['F'] = 100 # Number of graph convolutional filters
params['num_classes'] = num_classes
params['activation'] = "relu"
params['last_activation'] = "sigmoid"


### Train
n_folds = 10
skf = StratifiedKFold(n_splits=n_folds, random_state=811, shuffle=True)
train_accuracy = np.zeros(n_folds)
train_balanced_accuracy = np.zeros(n_folds)
accuracy = np.zeros(n_folds)
aucscore = np.zeros(n_folds)
balancedacc = np.zeros(n_folds)
f1 = np.zeros(n_folds)
timetrain = np.zeros(n_folds)
timepred = np.zeros(n_folds)
for fold, (train_index, test_index) in enumerate(skf.split(X, y)):
  import gc
  gc.collect()
    
  print(f"Fold = {fold}")
    
  xtrain = X[train_index,:]
  xtest = X[test_index,:]
  ytrain = y[train_index]
  ytest = y[test_index]
  start = time.time()
  if modelType=="GNN":
        model = run_GNN_patient(train_index, ytrain, graph_info, params)
  elif modelType=="DNN":
        model = run_DNN(xtrain, ytrain, params)
  timetrain[fold]=time.time() - start
  startpred = time.time()
  if modelType=="GNN":
        ypred_proba=model.predict(x=np.array(test_index), verbose=0)
        train_ypred_proba=model.predict(x=np.array(train_index), verbose=0)
  elif modelType=="DNN":  
        ypred_proba=model.predict(x=xtest, verbose=0)
        train_ypred_proba=model.predict(x=xtrain, verbose=0)
  ypred=np.argmax(ypred_proba, axis=1)
  train_ypred=np.argmax(train_ypred_proba, axis=1)
  
  train_accuracy[fold]=accuracy_score(ytrain, train_ypred)
  train_balanced_accuracy[fold]=balanced_accuracy_score(ytrain, train_ypred)
  
  accuracy[fold]=accuracy_score(ytest, ypred)
  aucscore[fold]=roc_auc_score(ytest, ypred_proba[:,1], average = "macro", multi_class = "ovr")
  balancedacc[fold]=balanced_accuracy_score(ytest, ypred)
  f1[fold]=f1_score(ytest, ypred, average = "binary")
  timepred[fold]=time.time() - startpred

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore,
'train_accuracy': train_accuracy, 'train_balanced_accuracy': train_balanced_accuracy}


### Export
if scaled == 0:
    if randomGraph == 0:
        filehandler = open("../../results/BreastCancer/"+modelType+"_patientScores_2023-11-09.obj", "wb")
    else:
        filehandler = open("../../results/BreastCancer/"+modelType+"_patientScores_randomGraph_2023-11-09.obj", "wb")
else:
    if randomGraph == 0:
        filehandler = open("../../results/BreastCancer/"+modelType+"_patientScores_scaled_2023-11-09.obj", "wb")
    else:
        filehandler = open("../../results/BreastCancer/"+modelType+\
        	"_patientScores_scaled_randomGraph_2023-11-09.obj", "wb")         

pickle.dump(scores, filehandler)
filehandler.close()






