import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_validate
import pickle
import time


# A. Basic perceptron ###
## 2. with scaled data (centering and scaling to unit variance) ###
labels = pd.read_csv("../../data/BreastCancer/labels_GEO_HG.csv")
labels = labels.transpose().to_numpy().ravel()

pseudoCountScaled = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI_scaled.csv", index_col=0)

PC = MLPClassifier(random_state=1647)
all_folds = StratifiedKFold(n_splits=10, shuffle=True, random_state=811)
scores = cross_validate(PC, X=pseudoCountScaled, y=labels, cv=all_folds, n_jobs=1, scoring=("accuracy", "balanced_accuracy", "roc_auc"), return_train_score=True)

filehandler = open("../../results/BreastCancer/PerceptronScores_scaled_2021-11-25.obj", "wb") 
pickle.dump(scores, filehandler)
