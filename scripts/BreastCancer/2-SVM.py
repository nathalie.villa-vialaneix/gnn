import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_validate
import pickle
import time

## 1. with unscaled data (minimum per row removed) ###
pseudoCounts = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI.csv")
pseudoCounts.index = list(pseudoCounts.pop("probe"))
pseudoCounts = pseudoCounts.transpose()
pseudoCounts = pseudoCounts.values.astype(np.float64)
pseudoCounts = pseudoCounts - np.min(pseudoCounts) # Making data lying in the interval [0, 8.35]

labels = pd.read_csv("../../data/BreastCancer/labels_GEO_HG.csv")
labels = labels.transpose().to_numpy().ravel()

svc = svm.SVC()
all_folds = StratifiedKFold(n_splits=10, shuffle=True, random_state=811)
scores = cross_validate(svc, X=pseudoCounts, y=labels, cv=all_folds, n_jobs=1, scoring=("accuracy", "balanced_accuracy", "roc_auc"), return_train_score=True)

filehandler = open("../../results/BreastCancer/SVCScores_2021-12-08.obj", "wb") 
pickle.dump(scores, filehandler)

