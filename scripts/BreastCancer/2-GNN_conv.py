# GNN script: using different convolutional layers with GNN

import sys
sys.path.append('../GNN_spektral')
import os
import numpy as np
import tensorflow as tf
import spektral
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, roc_auc_score
from graph_lrp_master.lib import coarsening, graph
from models import GNNLearning
import pickle
import argparse

parser = argparse.ArgumentParser(description='GNN applied to breast cancer dataset.')
parser.add_argument('--graph', type=str, help='Name of the file containing the graph (HPRD_PPI, HPRD_ct, HPRD_complete or HPRD_random).')
parser.add_argument('--scaled', type=int, help='indicates if scaling is used on features (1: scaling; 0: no scaling).')
parser.add_argument('--conv', type=str, help='Convolutional layer  (graphsage, gat, gcn, chebnet).')

# Get the arguments from the command line
args = parser.parse_args()

# Parameters

params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 109
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 1 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['decay_rate'] = 0.95 # Base of exponential decay. No decay with 1.
params['reg_lambda'] = 1e-4 # regularization parameter
params['momentum'] = 0 # 0 indicates no momentum
params['activation'] = "relu"
params['last_activation'] = "softmax"
params['conv'] = args.conv # convolutional layer

# Parameters of the GNN architecture
params['F'] = [32, 32] # Number of graph convolutional filters
params['K'] = [8, 8] # Polynomial orders
params['p'] = [2, 2] # Pooling sizes
params['M'] = [512, 128, 2] # Output dimensionality of fully connected layers
params['pool1'] = 'mpool1'

# Loading data
if args.scaled == 0:
    X = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI.csv")
    cols = X.columns.tolist()
    X = X[cols[:-1]]
    X = X.transpose()
    X = X.values.astype(np.float64)
    X = X - np.min(X) # Making data lying in the interval [0, 8.35]

if args.scaled == 1:
    X = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI_scaled.csv",index_col=0)
    X = X.values.astype(np.float64)

y = pd.read_csv("../../data/BreastCancer/labels_GEO_HG.csv")
y = y.values[0].astype(int)

if args.graph == "HPRD_PPI":
    A = pd.read_csv("../../data/BreastCancer/"+args.graph+".csv")
else:
    A = pd.read_csv("../../data/BreastCancer/"+args.graph+".csv", sep=" ")
A = csr_matrix(A.values.astype(np.float64).astype(np.float32))

# Coarsening the graph
graphs, perm = coarsening.coarsen(A, levels=2, self_connections=False)
X = coarsening.perm_data(X, perm) # rearrange the vertices in order to have a binary tree structure

# Preprocessing of the adjacency matrices in graphs depending on the convolution used
if params['conv'] =="chebnet":
    L = [spektral.layers.ChebConv.preprocess(A) for A in graphs] # normalized Laplacian

if params['conv'] == "gcn":
    L = [spektral.layers.GCNConv.preprocess(A) for A in graphs] # normalized adjacency matrix
    
if params['conv'] == "graphsage" or params['conv'] == "gat" :
    L = [A for A in graphs]
    
if args.graph == "HPRD_ct" or args.graph == "HPRD_complete":
    L = [A.todense() for A in L] # convert to dense because there are too many non zero values
    L = [tf.constant(A) for A in L] # convert to tensor
else:
    L = [spektral.utils.sparse.sp_matrix_to_sp_tensor(A) for A in L] # convert to sparse tensor

del graphs
del A

# Creating a custom Spektral dataset
class BreastCancerDataset(spektral.data.dataset.Dataset):
    def read(self):
        self.a = L[0]
        return [spektral.data.graph.Graph(x=X[i].reshape(X[i].size,1), y=y[i]) for i in range(X.shape[0])]
        
data = BreastCancerDataset()

# Cross-validation
n_folds = 10
acc_test = np.zeros(n_folds)
acc_bal_test = np.zeros(n_folds)
acc_train = np.zeros(n_folds)
acc_bal_train = np.zeros(n_folds)
roc_auc_test = np.zeros(n_folds)
fit_time = np.zeros(n_folds)
score_time = np.zeros(n_folds)
skf = StratifiedKFold(n_splits=n_folds, random_state=811, shuffle=True)

for fold, (train_index, test_index) in enumerate(skf.split(X, y)):
    print(f"Fold = {fold}")

    data_train = data[train_index]
    if data_train.n_graphs > 872 :
        data_train = data_train[:872]
    data_test = data[test_index]
    
    params['decay_steps'] = len(data_train) / params['batch_size']
    
    # Training GNN and Testing
    fit_time[fold], score_time[fold], y_test_true, y_test_pred, y_test_score, y_train_true, y_train_pred = GNNLearning(data_train, data_test, L, params)
    
    acc_test[fold] = accuracy_score(y_test_true, y_test_pred)
    acc_bal_test[fold] = balanced_accuracy_score(y_test_true, y_test_pred)
    roc_auc_test[fold] = roc_auc_score(y_test_true, y_test_score)
    acc_train[fold] = accuracy_score(y_train_true, y_train_pred)
    acc_bal_train[fold] = balanced_accuracy_score(y_train_true, y_train_pred)

    print(f"Train:  Acc = {acc_train[fold]:.3f} ; Balanced acc = {acc_bal_train[fold]:.3f}")
    print(f"Test:  Acc = {acc_test[fold]:.3f} ; Balanced acc = {acc_bal_test[fold]:.3f} ; AUROC = {roc_auc_test[fold]:.3f}")

score = {'fit_time':fit_time,'score_time':score_time,'test_accuracy':acc_test,'test_balanced_accuracy':acc_bal_test,'test_roc_auc':roc_auc_test, 'train_accuracy':acc_train, 'train_balanced_accuracy':acc_bal_train}

# Save results
if args.scaled == 1:
    results_filename ='../../results/BreastCancer/GNN_'+args.graph+'_'+args.conv+'Scores_scaled_2023-11-09.obj'
else:
    results_filename ='../../results/BreastCancer/GNN_'+args.graph+'_'+args.conv+'Scores_2023-11-09.obj'
with open(results_filename, 'wb') as handle:
    pickle.dump(score, handle)
