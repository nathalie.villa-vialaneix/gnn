# Settings #####
setwd("../..")
library("e1071")
library("pROC")
library("profmem")
cur_date <- "2023-01-26"

# Loading data (scaled) ####
# loading expression data: samples in columns
expr_file <- "data/BreastCancer/GEO_HG_PPI_scaled.csv"
expr <- read.table(expr_file, header = TRUE, sep = ",", row.names = 1)
expr <- as.matrix(expr)

# loading labels
labels <- read.table("data/BreastCancer/labels_GEO_HG.csv", header = TRUE,
                     sep = ",")
labels <- unlist(labels)

# loading folds
all_folds <- read.table("data/BreastCancer/ind_fold.txt", header = TRUE, 
                        sep = " ")
all_folds <- unlist(all_folds)

# CV folds ####
n <- length(labels)
folds <- lapply(0:9, function(ind) { indices <- which(all_folds == ind) })

# CV training
out_file <- paste0("RSVMScores_scaled_", cur_date, ".csv")
out_file <- file.path("results/BreastCancer", out_file)
mem_file <- gsub("\\.csv", "\\.dat", gsub("Scores", "Mem", out_file))

if (!file.exists(out_file)) {
  train_time <- rep(0, 10)
  test_time <- rep(0, 10)
  accuracy <- rep(0, 10)
  train_accuracy <- rep(0, 10)
  bal_accuracy <- rep(0, 10)
  train_bal_acc <- rep(0, 10)
  auc_roc <- rep(0, 10)
  
  totalmem <- total(profmem({
    for (ind in 1:10) {
      cat("Processing fold...", ind)
      train_time[ind] <- system.time({
        training_obs <- setdiff(1:nrow(expr), folds[[ind]])
        x <- expr[training_obs, ]
        cur_mod <- svm(x, labels[training_obs], type = "C-classification",
                      gamma = 1/(ncol(x) * var(unlist(data.frame(x)))),
                      probability = TRUE)
      })[3]
      
      test_time[ind] <- system.time({
        testing_obs <- folds[[ind]]
        cur_pred <- predict(cur_mod, expr[testing_obs, ])
        accuracy[ind] <- mean(cur_pred == labels[testing_obs])
        cur_pred_train <- predict(cur_mod, expr[training_obs, ])
        train_accuracy[ind] <- mean(cur_pred_train == labels[training_obs])
        conf_table <- table(cur_pred, labels[testing_obs])
        bal_accuracy[ind] <- mean(diag(conf_table) / apply(conf_table, 2, sum))
        conf_table <- table(cur_pred_train, labels[training_obs])
        train_bal_acc[ind] <- mean(diag(conf_table) / apply(conf_table, 2, sum))
        cur_scores <- predict(cur_mod, expr[testing_obs, ], probability = TRUE)
        cur_scores <- attr(cur_scores, "probabilities")[, 1]
        aucroc <- roc(as.factor(labels[testing_obs]), cur_scores, levels = c(0, 1),
                      direction = "<")                    
        auc_roc[ind] <- as.numeric(aucroc$auc)
      })[3]
    
    }
  }))
  
  all_res <- cbind(accuracy, bal_accuracy, train_accuracy, train_bal_acc,
                   auc_roc, train_time, test_time)
  names(all_res) <- c("accuracy", "balanced_accuracy", "training_accuracy",
                      "training_balanced_accuracy", "auc_roc", "training_time", 
                      "testing_time")
  
  write.csv(all_res, file = out_file, row.names = FALSE)
  write.table(totalmem, file = mem_file, row.names = FALSE, col.names = FALSE)
}
