"""
This file imports Breast Cancer PPI data and standardize features (zero mean and unit variance) before exporting the resulting CSV
Nathalie Vialaneix
"""

# required libraries
import pandas as pd
from sklearn import preprocessing

# read data
pseudoCount = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI.csv")
pseudoCount.index = list(pseudoCount.pop("probe"))
pseudoCount = pseudoCount.transpose()

# center and scale to unit variance
scaler = preprocessing.StandardScaler().fit(pseudoCount)
pseudoCountScaled = scaler.transform(pseudoCount)
pseudoCountScaled = pd.DataFrame(pseudoCountScaled)
pseudoCountScaled.index=pseudoCount.index
pseudoCountScaled.columns=pseudoCount.columns

# sanity checks
#>>> pseudoCountScaled.mean(axis=0)
#RPL41     -1.367703e-13
#EEF1A1     2.349502e-14
#TPT1       1.835706e-14
#RPL23A    -2.851493e-14
#UBC       -3.803654e-14
               #...     
#POU1F1     4.345005e-14
#SLC22A3   -7.255691e-14
#RAG2      -7.122149e-14
#SI        -4.705931e-14
#MSTN      -3.634520e-14
#Length: 6888, dtype: float64
#>>> pseudoCountScaled.std(axis=0)
#RPL41      1.000516
#EEF1A1     1.000516
#TPT1       1.000516
#RPL23A     1.000516
#UBC        1.000516
             #...   
#POU1F1     1.000516
#SLC22A3    1.000516
#RAG2       1.000516
#SI         1.000516
#MSTN       1.000516
#Length: 6888, dtype: float64

pseudoCountScaled.to_csv('../../data/BreastCancer/GEO_HG_PPI_scaled.csv', index = True)
# properly reread with:
# pd.read_csv("../../data/BreastCancer/GEO_HG_PPI_scaled.csv", index_col=0)
