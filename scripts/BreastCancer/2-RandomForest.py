import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_validate
import pickle
import time

## 1. with unscaled data (minimum per row removed) ###
pseudoCounts = pd.read_csv("../../data/BreastCancer/GEO_HG_PPI.csv")
pseudoCounts.index = list(pseudoCounts.pop("probe"))
pseudoCounts = pseudoCounts.transpose()
pseudoCounts = pseudoCounts.values.astype(np.float64)
pseudoCounts = pseudoCounts - np.min(pseudoCounts) # Making data lying in the interval [0, 8.35]

labels = pd.read_csv("../../data/BreastCancer/labels_GEO_HG.csv")
labels = labels.transpose().to_numpy().ravel()

RFC = RandomForestClassifier(n_estimators=500, min_samples_split=2, random_state=1647, bootstrap=False, n_jobs=-1)
all_folds = StratifiedKFold(n_splits=10, shuffle=True, random_state=811)
scores = cross_validate(RFC, X=pseudoCounts, y=labels, cv=all_folds, n_jobs=1, scoring=("accuracy", "balanced_accuracy", "roc_auc"), return_train_score=True)

filehandler = open("../../results/BreastCancer/RFScores_2021-11-25.obj", "wb") 
pickle.dump(scores, filehandler)

