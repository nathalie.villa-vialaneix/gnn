# GNN modified from https://keras.io/examples/graph/gnn_citations/

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow import keras
from tensorflow.keras.callbacks import EarlyStopping

def run_GNN_patient(trainIdx, y_train, graph_info, params):
    model = GNNNodeClassifier(
        graph_info=graph_info,
        num_classes=params['num_classes'],
        hidden_units=[params['F']],
        dropout_rate=params['dropout'],
        reg_lambda=params['reg_lambda'],
        activation=params['activation'],
        last_activation=params['last_activation'],
        name="gnn_model",
    )

    print("GNN output shape:", model([1]))

    #gnn_model.build()
    model.summary()
    
    # Compile the model.
    if params['num_classes']>1:
    	model.compile(
            optimizer=keras.optimizers.Adam(params['learning_rate'],epsilon=1e-08),
            loss=keras.losses.SparseCategoricalCrossentropy(),
            metrics=["sparse_categorical_accuracy"],
    	)
    elif params['num_classes']==1:
    	model.compile(
            optimizer=keras.optimizers.Adam(params['learning_rate'],epsilon=1e-08),
            loss=keras.losses.MeanSquaredError(),
            metrics=['mse', 'mae', 'mape'],
    	)
    
    # Fit the model.
    history = model.fit(
        x=trainIdx,
        y=y_train,
        epochs=params['epochs'],
        batch_size=params['batch_size'],
        validation_split=0.1,
        callbacks=[EarlyStopping(patience=5, restore_best_weights=True)],
    )
    return model


def run_DNN(xtrain, y_train, params):

    X_in = layers.Input((xtrain.shape[1],))
    x = layers.Dense(params['F'], activation=params['activation'],
    	kernel_regularizer=regularizers.L2(l2=params['reg_lambda']))(X_in)
    x = layers.Dropout(params['dropout'])(x)
    x = layers.BatchNormalization()(x)
    output = layers.Dense(params['num_classes'], activation=params['last_activation'],
    	kernel_regularizer=regularizers.L2(l2=params['reg_lambda']))(x)
    model = keras.Model(X_in, output)

    #gnn_model.build()
    model.summary()

    # Compile the model.
    if params['num_classes']>1:
    	model.compile(
            optimizer=keras.optimizers.Adam(params['learning_rate'],epsilon=1e-08),
            loss=keras.losses.SparseCategoricalCrossentropy(),
            metrics=["sparse_categorical_accuracy"],
    	)
    elif params['num_classes']==1:
    	model.compile(
            optimizer=keras.optimizers.Adam(params['learning_rate'],epsilon=1e-08),
            loss=keras.losses.MeanSquaredError(),
            metrics=['mse', 'mae', 'mape'],
    	)

    # Fit the model.
    history = model.fit(
        x=xtrain,
        y=y_train,
        epochs=params['epochs'],
        batch_size=params['batch_size'],
        validation_split=0.1,
        callbacks=[EarlyStopping(patience=5, restore_best_weights=True)],
    )
    return model


# GNN functions
class GNNNodeClassifier(tf.keras.Model):
    def __init__(
        self,
        graph_info,
        num_classes,
        hidden_units,
        aggregation_type="mean",
        combination_type="concat",
        dropout_rate=0.2,
        reg_lambda=1e-4,
        activation="relu",
        last_activation="softmax",
        normalize=True,
        *args,
        **kwargs,
    ):
        super(GNNNodeClassifier, self).__init__(*args, **kwargs)

        # Unpack graph_info to three elements: node_features, edges, and edge_weight.
        node_features, edges, edge_weights = graph_info
        self.node_features = node_features
        self.edges = edges
        self.edge_weights = edge_weights
        # Set edge_weights to ones if not provided.
        if self.edge_weights is None:
            self.edge_weights = tf.ones(shape=edges.shape[1])
        # Scale edge_weights to sum to 1.
        self.edge_weights = self.edge_weights / tf.math.reduce_sum(self.edge_weights)

        # Create the GraphConv layer.
        self.conv1 = GraphConvLayer(
            hidden_units=hidden_units,
            dropout_rate=dropout_rate,
            reg_lambda=reg_lambda,
            aggregation_type=aggregation_type,
            combination_type=combination_type,
            normalize=normalize,
            name="graph_conv1",
        )
        # Create a compute logits layer.
        self.dense = layers.Dense(num_classes, activation=last_activation,
        	kernel_regularizer=regularizers.L2(l2=reg_lambda))
        
    def call(self, input_node_indices):
        # Apply the first graph conv layer.
        x = self.conv1((self.node_features, self.edges, self.edge_weights))
        # Fetch node embeddings for the input node_indices.
        node_embeddings = tf.gather(x, input_node_indices)
        out = self.dense(node_embeddings)
        return out


class GraphConvLayer(layers.Layer):
    def __init__(
        self,
        hidden_units,
        dropout_rate=0.5,
        reg_lambda=1e-4,
        activation="relu",
        aggregation_type="mean",
        combination_type="concat",
        normalize=False,
        *args,
        **kwargs,
    ):
        super(GraphConvLayer, self).__init__(*args, **kwargs)

        self.aggregation_type = aggregation_type
        self.combination_type = combination_type
        self.normalize = normalize
        
        self.update_fn = create_ffn(hidden_units=hidden_units, dropout_rate=dropout_rate, 
        	reg_lambda=reg_lambda,activation=activation)

    def aggregate(self, node_indices, neighbour_messages, node_repesentations):
        # node_indices shape is [num_edges].
        # neighbour_messages shape: [num_edges, representation_dim].
        # node_repesentations shape is [num_nodes, representation_dim]
        num_nodes = node_repesentations.shape[0]
        if self.aggregation_type == "sum":
            aggregated_message = tf.math.unsorted_segment_sum(
                neighbour_messages, node_indices, num_segments=num_nodes
            )
        elif self.aggregation_type == "mean":
            aggregated_message = tf.math.unsorted_segment_mean(
                neighbour_messages, node_indices, num_segments=num_nodes
            )
        elif self.aggregation_type == "max":
            aggregated_message = tf.math.unsorted_segment_max(
                neighbour_messages, node_indices, num_segments=num_nodes
            )
        else:
            raise ValueError(f"Invalid aggregation type: {self.aggregation_type}.")

        return aggregated_message

    def update(self, node_repesentations, aggregated_messages):
        # node_repesentations shape is [num_nodes, representation_dim].
        # aggregated_messages shape is [num_nodes, representation_dim].
        if self.combination_type == "concat":
            # Concatenate the node_repesentations and aggregated_messages.
            h = tf.concat([node_repesentations, aggregated_messages], axis=1)
        elif self.combination_type == "add":
            # Add node_repesentations and aggregated_messages.
            h = node_repesentations + aggregated_messages
        else:
            raise ValueError(f"Invalid combination type: {self.combination_type}.")

        # Apply the processing function.
        node_embeddings = self.update_fn(h)
        if self.normalize:
            node_embeddings = tf.nn.l2_normalize(node_embeddings, axis=-1)
        return node_embeddings

    def call(self, inputs):
        """Process the inputs to produce the node_embeddings.

        inputs: a tuple of three elements: node_repesentations, edges, edge_weights.
        Returns: node_embeddings of shape [num_nodes, representation_dim].
        """

        node_repesentations, edges, edge_weights = inputs
        # Get node_indices (source) and neighbour_indices (target) from edges.
        node_indices, neighbour_indices = edges[0], edges[1]
        # neighbour_repesentations shape is [num_edges, representation_dim].
        neighbour_repesentations = tf.gather(node_repesentations, neighbour_indices)

        # Prepare the messages of the neighbours.
        #neighbour_messages = self.prepare(neighbour_repesentations, edge_weights)
        neighbour_messages=neighbour_repesentations
        # Aggregate the neighbour messages.
        aggregated_messages = self.aggregate(
            node_indices, neighbour_messages, node_repesentations
        )
        # Update the node embedding with the neighbour messages.
        return self.update(node_repesentations, aggregated_messages)
    
    
def create_ffn(hidden_units, dropout_rate, reg_lambda, activation, name=None):
    fnn_layers = []
    
    for units in hidden_units:
        fnn_layers.append(layers.Dense(units, activation=activation,
        	kernel_regularizer=regularizers.L2(l2=reg_lambda)))
        fnn_layers.append(layers.Dropout(dropout_rate))
        fnn_layers.append(layers.BatchNormalization())

    return keras.Sequential(fnn_layers, name=name)




