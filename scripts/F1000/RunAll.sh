#! /bin/bash
python3 0-formatHDF2CSV.py

mprof run -T 5 -C -o perceptron_fullmoa.dat 1-Perceptron_fullmoa.py
mprof run -T 5 -C -o perceptronalt_fullmoa.dat 1-PerceptronAlt_fullmoa.py
mprof run -T 5 -C -o perceptron_primarysite.dat 1-Perceptron_fullprimarysite.py
mprof run -T 5 -C -o perceptronalt_primarysite.dat 1-PerceptronAlt_fullprimarysite.py
mprof run -T 5 -C -o perceptron_fullsubtype.dat 1-Perceptron_fullsubtype.py
mprof run -T 5 -C -o perceptronalt_fullsubtype.dat 1-PerceptronAlt_fullsubtype.py
mprof run -T 5 -C -o perceptron_moa.dat 1-Perceptron_moa.py
mprof run -T 5 -C -o perceptronalt_moa.dat 1-PerceptronAlt_moa.py

mprof run -T 5 -C -o randomforest_fullmoa.dat 1-RandomForest_fullmoa.py
mprof run -T 5 -C -o randomforestalt_fullmoa.dat 1-RandomForestAlt_fullmoa.py
mprof run -T 5 -C -o randomforest_fullprimarysite.dat 1-RandomForest_fullprimarysite.py
mprof run -T 5 -C -o randomforestalt_fullprimarysite.dat 1-RandomForestAlt_fullprimarysite.py
mprof run -T 5 -C -o randomforest_fullsubtype.dat 1-RandomForest_fullsubtype.py
mprof run -T 5 -C -o randomforestalt_fullsubtype.dat 1-RandomForestAlt_fullsubtype.py
mprof run -T 5 -C -o randomforest_moa.dat 1-RandomForest_moa.py
mprof run -T 5 -C -o randomforestalt_moa.dat 1-RandomForestAlt_moa.py

mprof run -T 5 -o svm_moa.dat 1-SVM_moa.py

R --vanilla -s -f 1-RandomForest_fullmoa.R
R --vanilla -s -f 1-RandomForest_moa.R
R --vanilla -s -f 1-RandomForest_primarysite.R
R --vanilla -s -f 1-RandomForest_subtype.R
R --vanilla -s -f 1-SVM_moa.R

python3 2-formatCSV2OJB.py

mprof run -T 5 -C -o gnn_full_moa.dat 1-GNN.py --data prostate --network full --target moa
mprof run -T 5 -C -o gnn_prostate_moa.dat 1-GNN.py --data prostate --network prostate --target moa
mprof run -T 5 -C -o gnn_fullmoa.dat 1-GNN.py --data full --network full --target moa
mprof run -T 5 -C -o gnn_fullsubtype.dat 1-GNN.py --data full --network full --target subtype
mprof run -T 5 -C -o gnn_fullprimarysite.dat 1-GNN.py --data full --network full --target primary_site

mprof run -T 5 -C -o gnnpatient_moa.dat 1-GNN_patients_moa.py --modelType GNN --randomGraph 0
mprof run -T 5 -C -o gnnpatient_dnn_moa.dat 1-GNN_patients_moa.py --modelType DNN --randomGraph 0

mprof run -T 5 -C -o gnnpatient_dnn_fullmoa.dat 1-GNN_patients_fullmoa.py --modelType DNN --graph 0
mprof run -T 5 -C -o gnnpatient_dnn_fullprimarysite.dat 1-GNN_patients_fullprimarysite.py --modelType DNN --graph 0
mprof run -T 5 -C -o gnnpatient_dnn_fullsubtype.dat 1-GNN_patients_fullsubtype.py --modelType DNN --graph 0
