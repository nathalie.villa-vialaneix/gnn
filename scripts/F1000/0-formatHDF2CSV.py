import pandas as pd

## MOA prostate
full_data = pd.read_hdf("../../data/F1000/GSE92742_fully_restricted_prostate.hdf")

full_data.to_csv("../../data/F1000/GSE92742_fully_restricted_prostate.csv", header=True, index=False)

labels = full_data.index.get_level_values("moa").to_series()
labels = labels.to_numpy().ravel()
pd.DataFrame(labels).to_csv("../../data/F1000/GSE92742_labels_restricted_prostate.csv", header=False, index=False)

fold_indices = full_data.index.get_level_values("Fold").to_series()
fold_indices = fold_indices.ravel()
pd.DataFrame(fold_indices).to_csv("../../data/F1000/GSE92742_folds_restricted_prostate.csv", header=False, index=False)

## full MOA / subtype / 
full_data = pd.read_hdf("../../data/F1000/GSE92742_fully_restricted.hdf")

full_data.to_csv("../../data/F1000/GSE92742_fully_restricted.csv", header=True, index=False)

labels = full_data.index.get_level_values("moa").to_series()
labels = labels.to_numpy().ravel()
pd.DataFrame(labels).to_csv("../../data/F1000/GSE92742_moa_restricted.csv", header=False, index=False)

labels = full_data.index.get_level_values("subtype").to_series()
labels = labels.to_numpy().ravel()
pd.DataFrame(labels).to_csv("../../data/F1000/GSE92742_subtype_restricted.csv", header=False, index=False)

labels = full_data.index.get_level_values("primary_site").to_series()
fold_indices = full_data.index.get_level_values("Fold").to_series()
labels = labels.to_numpy().ravel()
pd.DataFrame(labels).to_csv("../../data/F1000/GSE92742_primarysite_restricted.csv", header=False, index=False)

fold_indices = full_data.index.get_level_values("Fold").to_series()
fold_indices = fold_indices.ravel()
pd.DataFrame(fold_indices).to_csv("../../data/F1000/GSE92742_folds_restricted.csv", header=False, index=False)
