#!/usr/bin/env python
# coding: utf-8


# Training of graph neural network (GNN) whose convolution is done over the patients.
# Training of dense neural network (DNN) is also included.

import os
os.getcwd()


import pandas as pd
import numpy as np
from sklearn import svm, preprocessing
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, roc_curve, auc, f1_score, balanced_accuracy_score, roc_auc_score
import pickle
import time
import argparse

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import keras
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping
from scipy import sparse
from sklearn.metrics.pairwise import euclidean_distances

import sys
sys.path.append('../GNN_keras')
from models_gnn_patients import *

#tf.keras.utils.set_random_seed(seed=100)
tf.random.set_seed(seed=100)

print(tf.__version__)



# Parameters
parser = argparse.ArgumentParser(description='GNN applied to F1000 dataset.')
parser.add_argument('--modelType', type=str, help='GNN or DNN')
parser.add_argument('--randomGraph', type=int, help='indicates if random graph is used (2: complete graph; 1: random graph; 0: graph estimated from data).')

# Get the arguments from the command line
args = parser.parse_args()
modelType=args.modelType
randomGraph=args.randomGraph
print(modelType)
print(randomGraph)


## Load data
full_data = pd.read_hdf("../../data/F1000/GSE92742_fully_restricted_prostate.hdf")
labels = full_data.index.get_level_values("moa").to_series()
fold_indices = full_data.index.get_level_values("Fold").to_series()
labels = labels.to_numpy().ravel()
fold_indices = fold_indices.ravel()

num_classes = len(np.unique(labels))

le = preprocessing.LabelEncoder()
le.fit(labels)
labels = le.transform(labels)



# Make similarity graph between patients
if randomGraph == 0:
    A=euclidean_distances(full_data)
    A=A/np.max(A)
    A=1-A
    np.fill_diagonal(A,0)
    A[A<np.quantile(A,0.998)]=0
    A[A>=np.quantile(A,0.998)]=1
    file_graph = "../../data/F1000/graph_patient.csv" 
    pd.DataFrame(A).to_csv(file_graph, header=False, index=False)

if randomGraph == 2:
    A = np.ones((full_data.shape[0],full_data.shape[0]))


# Make data for the graph convolutional network
edges=sparse.csr_matrix(A)
edges=np.stack((edges.nonzero()[0],edges.nonzero()[1]),axis=0)
print(edges[:,0:10])
# Create an edge weights array of ones.
edge_weights = tf.ones(shape=edges.shape[1])
# Create a node features array of shape [num_nodes, num_features].
node_features = tf.cast(full_data.to_numpy(), dtype=tf.dtypes.float32)
# Create graph info tuple with node_features, edges, and edge_weights.
graph_info = (node_features, edges, edge_weights)

print("Edges shape:", edges.shape)
print("Nodes shape:", node_features.shape)


# Parameters
params = dict()
params['epochs'] = 100 # number of training epochs
params['batch_size'] = 200
params['learning_rate'] = 0.001 # Initial learning rate
params['dropout'] = 0 # in dense layers : probability to keep hidden neurones. No dropout with 1
params['reg_lambda'] = 1e-4 # regularization parameter
params['F'] = 100 # Number of graph convolutional filters
params['num_classes'] = num_classes
params['activation'] = "relu"
params['last_activation'] = "softmax"


### Train
skf = GroupKFold(n_splits=10)
n_folds=10
train_accuracy = []
train_balanced_accuracy = []
accuracy = []
aucscore = []
balancedacc = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(skf.split(full_data, labels, fold_indices)):
  import gc
  gc.collect()
    
  xtrain = full_data.iloc[train_index,:].to_numpy()
  xtest = full_data.iloc[test_index,:].to_numpy()
  ytrain = labels[train_index]
  ytest = labels[test_index]
  start = time.time()
  if modelType=="GNN":
        model = run_GNN_patient(train_index, ytrain, graph_info, params)
  elif modelType=="DNN":
        model = run_DNN(xtrain, ytrain, params)
  timetrain.append(time.time() - start)
  startpred = time.time()
  if modelType=="GNN":
        ypred_proba=model.predict(x=np.array(test_index), verbose=0)
        train_ypred_proba=model.predict(x=np.array(train_index), verbose=0)                        
  elif modelType=="DNN":  
        ypred_proba=model.predict(x=xtest, verbose=0)   
        train_ypred_proba=model.predict(x=xtrain, verbose=0)                                    
  ypred=np.argmax(ypred_proba, axis=1)
  train_ypred=np.argmax(train_ypred_proba, axis=1)  
  
  train_accuracy.append(accuracy_score(ytrain, train_ypred))  
  train_balanced_accuracy.append(balanced_accuracy_score(ytrain, train_ypred))  
    
  accuracy.append(accuracy_score(ytest, ypred))
  aucscore.append(roc_auc_score(ytest, ypred_proba, average = "macro", multi_class = "ovr"))
  balancedacc.append(balanced_accuracy_score(ytest, ypred))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore,
'train_accuracy': train_accuracy, 'train_balanced_accuracy': train_balanced_accuracy}


### Export
filehandler = open("../../results/F1000/"+modelType+"_patient_Scores_2022-09-12.obj", "wb") 
pickle.dump(scores, filehandler)
filehandler.close()




