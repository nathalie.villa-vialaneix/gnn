# Script for running GNN on F1000 dataset

import sys
sys.path.append('../GNN_spektral')
import os
import numpy as np
import tensorflow as tf
import spektral
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, roc_auc_score, confusion_matrix
from graph_lrp_master.lib import coarsening, graph
from models import GNNLearning
import pickle
import argparse
import scipy.io as sio
import h5py
import random

random.seed(10)


parser = argparse.ArgumentParser(description='GNN applied to F1000 dataset.')
parser.add_argument('--data', type=str, help='full or prostrate.')
parser.add_argument('--network', type=str, help='full or prostrate.')
parser.add_argument('--target', type=str, help='moa, subtype or primary_site (only moa for prostate dataset).')

# Get the arguments from the command line
args = parser.parse_args()

# Parameters (taken from the article)

if args.data == "prostate": # the only possible target is moa

    params = dict()
    params['reg_lambda'] = 4.0e-3 # regularization parameter
    params['epochs'] = 200 # number of training epochs
    params['batch_size'] = 55
    params['learning_rate'] = 5e-3 # Initial learning rate
    params['decay_steps'] = 415
    params['decay_rate'] = 9.50e-1 # Base of exponential decay. No decay with 1.
    params['dropout'] = 5e-1 # in dense layers : probability to keep hidden neurones. No dropout with 1
    params['momentum'] = 9.70e-1 # 0 indicates no momentum
    params['activation'] = "relu"
    params['last_activation'] = "softmax"
    params['F'] = [25] # Number of graph convolutional filters
    params['K'] = [15] # Polynomial orders
    params['p'] = [2] # Pooling sizes
    params['M'] = [168, 14, 9] # Output dimensionality of fully connected layers
    params['pool1'] = 'mpool1'
    params['conv'] = "chebnet" # convolutional layer
    
else: # ie args.data = "full"

    if args.target == "moa":

        params = dict()
        params['reg_lambda'] = 0.01087902135723254 # regularization parameter
        params['epochs'] = 350 # number of training epochs
        params['batch_size'] = 92
        params['learning_rate'] = 0.0012265138868632257 # Initial learning rate
        params['decay_steps'] = 405
        params['decay_rate'] = 0.9914003298632604 # Base of exponential decay. No decay with 1.
        params['dropout'] = 0.6980409669798152 # in dense layers : probability to keep hidden neurones. No dropout with 1
        params['momentum'] = 0.8789501097227141 # 0 indicates no momentum
        params['activation'] = "relu"
        params['last_activation'] = "softmax"
        params['F'] = [9] # Number of graph convolutional filters
        params['K'] = [7] # Polynomial orders
        params['p'] = [2] # Pooling sizes
        params['M'] = [137, 49] # Output dimensionality of fully connected layers
        params['pool1'] = 'apool1'
        params['conv'] = "chebnet" # convolutional layer
    
    elif args.target == "subtype":

        params = dict()
        params['reg_lambda'] = 0.005416198768028686 # regularization parameter
        params['epochs'] = 300 # number of training epochs
        params['batch_size'] = 88
        params['learning_rate'] = 0.0029527675535869005 # Initial learning rate
        params['decay_steps'] = 362
        params['decay_rate'] = 0.9761903148334647 # Base of exponential decay. No decay with 1.
        params['dropout'] = 0.4542127808744735 # in dense layers : probability to keep hidden neurones. No dropout with 1
        params['momentum'] = 0.972982486521958 # 0 indicates no momentum
        params['activation'] = "relu"
        params['last_activation'] = "softmax"
        params['F'] = [43] # Number of graph convolutional filters
        params['K'] = [8] # Polynomial orders
        params['p'] = [2] # Pooling sizes
        params['M'] = [150, 150, 14] # Output dimensionality of fully connected layers
        params['pool1'] = 'mpool1'
        params['conv'] = "chebnet" # convolutional layer
    
    elif args.target == "primary_site":

        params = dict()
        params['reg_lambda'] = 0.0030802351761877 # regularization parameter
        params['epochs'] = 350 # number of training epochs
        params['batch_size'] = 68
        params['learning_rate'] = 0.003126101959941907 # Initial learning rate
        params['decay_steps'] = 380
        params['decay_rate'] = 0.9889322578788354 # Base of exponential decay. No decay with 1.
        params['dropout'] = 0.5622576841981273 # in dense layers : probability to keep hidden neurones. No dropout with 1
        params['momentum'] = 0.9446417604899967 # 0 indicates no momentum
        params['activation'] = "relu"
        params['last_activation'] = "softmax"
        params['F'] = [41] # Number of graph convolutional filters
        params['K'] = [5] # Polynomial orders
        params['p'] = [2] # Pooling sizes
        params['M'] = [135, 12] # Output dimensionality of fully connected layers
        params['pool1'] = 'apool1'
        params['conv'] = "chebnet" # convolutional layer


# Loading graph

if args.network =='prostate':
    filehandler = open("../../data/F1000/prostate_adjacency.pkl","rb")
    A = pickle.load(filehandler)
else:
    filehandler = open("../../data/F1000/gene_regulatory_network_adjacency.pkl","rb")
    A = pickle.load(filehandler)

# graph coarsening
graphs, perm = coarsening.coarsen(A, levels=len(params['p']), self_connections=False)
L = [spektral.layers.ChebConv.preprocess(A) for A in graphs]
L = [spektral.utils.sparse.sp_matrix_to_sp_tensor(A) for A in L] # convert to sparse tensor

del A
del graphs


# Loading gene expressions and labels
if args.data =='prostate':
    full_data = pd.read_hdf("../../data/F1000/GSE92742_fully_restricted_prostate.hdf")
else:
    full_data = pd.read_hdf("../../data/F1000/GSE92742_fully_restricted.hdf")

labels = full_data.index.get_level_values(args.target).to_series()
labels = labels.to_numpy().ravel()
# Convert string labels to int
le = preprocessing.LabelEncoder()
le.fit(labels)
y = le.transform(labels)

n_classes = len(np.unique(y))

fold_indices = full_data.index.get_level_values("Fold").to_series()
fold_indices = fold_indices.ravel()

X = full_data.to_numpy()
X = X.astype(np.float64)
X = coarsening.perm_data(X, perm)


# Creating a custom Spektral dataset
class BreastCancerDataset(spektral.data.dataset.Dataset):
    def read(self):
        self.a = L[0]
        return [spektral.data.graph.Graph(x=X[i].reshape(X[i].size,1), y=y[i]) for i in range(X.shape[0])]
        
data = BreastCancerDataset()

# Cross-validation
n_folds = 10
acc_test = np.zeros(n_folds)
acc_bal_test = np.zeros(n_folds)
roc_auc_test = np.zeros(n_folds)
acc_train = np.zeros(n_folds)
acc_bal_train = np.zeros(n_folds)
conf_matrix = np.zeros((n_classes, n_classes, n_folds))
fit_time = np.zeros(n_folds)
score_time = np.zeros(n_folds)

skf = GroupKFold(n_splits=n_folds)
for fold, (train_index, test_index) in enumerate(skf.split(X, y, fold_indices)):
    print(f"Fold = {fold}", flush=True)

    data_train = data[train_index]
    data_test = data[test_index]

    # Training GNN and Testing
    fit_time[fold], score_time[fold], y_test_true, y_test_pred, y_test_score, y_train_true, y_train_pred = GNNLearning(data_train, data_test, L, params)
    
    acc_test[fold] = accuracy_score(y_test_true, y_test_pred)
    acc_bal_test[fold] = balanced_accuracy_score(y_test_true, y_test_pred)
    roc_auc_test[fold] = roc_auc_score(y_test_true, y_test_score, average = "macro", multi_class="ovr")
    conf_matrix[:,:,fold] = confusion_matrix(y_test_true, y_test_pred)
    acc_train[fold] = accuracy_score(y_train_true, y_train_pred)
    acc_bal_train[fold] = balanced_accuracy_score(y_train_true, y_train_pred)
    
    print(f"Train:  Acc = {acc_train[fold]:.3f} ; Balanced acc = {acc_bal_train[fold]:.3f}")
    print(f"Test:  Acc = {acc_test[fold]:.3f} ; Balanced acc = {acc_bal_test[fold]:.3f} ; AUROC = {roc_auc_test[fold]:.3f}", flush=True)
    

# Save results
score = {'fit_time':fit_time,'score_time':score_time,'test_accuracy':acc_test,'test_balanced_accuracy':acc_bal_test,'test_roc_auc':roc_auc_test, 'conf_matrix': conf_matrix,'train_accuracy':acc_train, 'train_balanced_accuracy':acc_bal_train}

results_filename ='../../results/F1000/GNN_'+args.data+'_'+args.network+'_'+args.target+'_Scores_2023-11-09.obj'
with open(results_filename, 'wb') as handle:
    pickle.dump(score, handle)
