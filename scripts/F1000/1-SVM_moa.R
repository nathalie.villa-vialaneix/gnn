# Settings #####
setwd("../..")
library("e1071")
library("pROC")
library("profmem")
cur_date <- "2023-01-31"

# Loading data ####
# loading expression data
expr_file <- "data/F1000/GSE92742_fully_restricted_prostate.csv"
expr <- read.table(expr_file, header = TRUE, sep = ",")

# loading labels
labels <- read.table("data/F1000/GSE92742_labels_restricted_prostate.csv", 
                     header = FALSE, sep = "\t")
labels <- unlist(labels)
labels <- factor(labels)

# loading folds
all_folds <- read.table("data/F1000/GSE92742_folds_restricted_prostate.csv", 
                        header = FALSE, sep = "\t")
all_folds <- unlist(all_folds)

# CV folds ####
n <- length(labels)
folds <- lapply(0:9, function(ind) { indices <- which(all_folds == ind) })

# CV training
out_file <- paste0("RSVM_Scores_", cur_date, ".csv")
out_file <- file.path("results/F1000", out_file)
mem_file <- gsub("\\.csv", "\\.dat", gsub("Scores", "Mem", out_file))

if (!file.exists(out_file)) {
  train_time <- rep(0, 10)
  test_time <- rep(0, 10)
  accuracy <- rep(0, 10)
  bal_accuracy <- rep(0, 10)
  train_accuracy <- rep(0, 10)
  train_bal_acc <- rep(0, 10)
  auc_roc <- rep(0, 10)
  
  totalmem <- total(profmem({
    for (ind in 1:10) {
      cat("Processing fold...", ind)
      train_time[ind] <- system.time({
        training_obs <- setdiff(1:nrow(expr), folds[[ind]])
        x <- expr[training_obs, ]
        cur_mod <- svm(x, labels[training_obs], type = "C-classification", 
                      gamma = 1/(ncol(x) * var(unlist(data.frame(x)))),
                      probability = TRUE, scale=FALSE)
      })[3]
      
      test_time[ind] <- system.time({
        testing_obs <- folds[[ind]]
        cur_pred <- predict(cur_mod, expr[testing_obs, ])
        cur_pred_train <- predict(cur_mod, expr[training_obs, ])
        accuracy[ind] <- mean(cur_pred == labels[testing_obs])
        train_accuracy[ind] <- mean(cur_pred_train == labels[training_obs])
        conf_table <- table(cur_pred, labels[testing_obs])
        bal_accuracy[ind] <- mean(diag(conf_table) / apply(conf_table, 2, sum))
        conf_table <- table(cur_pred_train, labels[training_obs])
        train_bal_acc[ind] <- mean(diag(conf_table) / apply(conf_table, 2, sum))
        cur_scores <- predict(cur_mod, expr[testing_obs, ], probability = TRUE)
        cur_scores <- attr(cur_scores, "probabilities")
        aucroc <- multiclass.roc(labels[testing_obs], cur_scores)
        auc_roc[ind] <- as.numeric(aucroc$auc)
      })[3]
    
    }
  }))
  
  all_res <- cbind(accuracy, bal_accuracy, train_accuracy, train_bal_acc,
                   auc_roc, train_time, test_time)
  names(all_res) <- c("accuracy", "balanced_accuracy", "training_accuracy",
                      "training_balanced_accuracy", "auc_roc", "training_time", 
                      "testing_time")
  
  write.csv(all_res, file = out_file, row.names = FALSE)
  write.table(totalmem, file = mem_file, row.names = FALSE, col.names = FALSE)
}
