import pandas as pd
import pickle
import numpy as np

results = pd.read_csv("../../results/F1000/RRandomForest_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/F1000/RRandomForest_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/F1000/RSVM_Scores_2023-01-31.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/F1000/RSVM_Scores_2023-01-31.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/F1000/RRandomForest_full_moa_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/F1000/RRandomForest_full_moa_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/F1000/RRandomForest_full_primarysite_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/F1000/RRandomForest_full_primarysite_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

results = pd.read_csv("../../results/F1000/RRandomForest_full_subtype_Scores_2023-01-26.csv")
formatr = {"fit_time": results.transpose().to_numpy()[5], "score_time": results.transpose().to_numpy()[6], "test_accuracy": results.transpose().to_numpy()[0], "test_balanced_accuracy": results.transpose().to_numpy()[1], "test_roc_auc": results.transpose().to_numpy()[4], "train_accuracy": results.transpose().to_numpy()[2], "train_balanced_accuracy": results.transpose().to_numpy()[3]}
filehandler = open("../../results/F1000/RRandomForest_full_subtype_Scores_2023-01-26.obj", "wb") 
pickle.dump(formatr, filehandler)

## notes

