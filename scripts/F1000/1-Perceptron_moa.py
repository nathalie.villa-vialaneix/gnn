import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GroupKFold
from sklearn.metrics import accuracy_score, roc_curve, auc, f1_score, balanced_accuracy_score, roc_auc_score
import pickle
import time

## 1. with default their parameters - prostate / MOA
full_data = pd.read_hdf("../../data/F1000/GSE92742_fully_restricted_prostate.hdf")
labels = full_data.index.get_level_values("moa").to_series()
fold_indices = full_data.index.get_level_values("Fold").to_series()
labels = labels.to_numpy().ravel()
fold_indices = fold_indices.ravel()

skf = GroupKFold(n_splits=10)
n_folds=10
accuracy = []
train_acc = []
aucscore = []
balancedacc = []
train_bal_acc = []
timetrain = []
timepred = []
for fold, (train_index, test_index) in enumerate(skf.split(full_data, labels, fold_indices)):
  xtrain = full_data.iloc[train_index,:]
  xtest = full_data.iloc[test_index,:]
  ytrain = labels[train_index]
  ytest = labels[test_index]
  start = time.time()
  MLP = MLPClassifier(random_state=903, learning_rate="invscaling", nesterovs_momentum=True, hidden_layer_sizes=[997], learning_rate_init=0.0553037462469582, momentum=0.8670995345995152, early_stopping=False, alpha=0.8200640684863673, power_t=0.2260141186252257, activation="relu", beta_1=0.864394587085052, beta_2=0.9985546737363457, max_iter=350, epsilon=8.57966387882825e-09, solver="sgd").fit(xtrain, ytrain)
  timetrain.append(time.time() - start)
  startpred = time.time()
  accuracy.append(MLP.score(xtest, ytest))
  train_acc.append(MLP.score(xtrain, ytrain))
  ypred = MLP.predict_proba(xtest)
  aucscore.append(roc_auc_score(ytest, ypred, average = "macro", multi_class = "ovr"))
  ypred = MLP.predict(xtest)
  ypred_train = MLP.predict(xtrain)
  balancedacc.append(balanced_accuracy_score(ytest, ypred))
  train_bal_acc.append(balanced_accuracy_score(ytrain, ypred_train))
  timepred.append(time.time() - startpred)

scores = {'fit_time': timetrain, 'score_time':timepred, 'test_accuracy':accuracy, 'test_balanced_accuracy':balancedacc, 'test_roc_auc':aucscore, 'train_accuracy': train_acc, 'train_balanced_accuracy': train_bal_acc}

### Export
filehandler = open("../../results/F1000/MLP_Scores_2022-08-30.obj", "wb") 
pickle.dump(scores, filehandler)
