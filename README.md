# Benchmarking GNN for phenotype prediction

**Reference**: Brouard, C., Mourad, R., & Vialaneix, N. (2024). Should we really use graph neural networks for transcriptomic prediction? *Briefings in Bioinformatics*, **25**(2), bbae027. DOI: http://dx.doi.org/10.1093/bib/bbae027

This repository is a replication repository for this article "Should we really use graph neural networks for transcriptomic prediction?". 

## How to use the repository?

### Data

Populate the `data` directory by downloading datasets referenced in the README file found in this repository.

### Scripts

All subdirectories `scripts/*` with `*` in {`BreastCancer`, `CancerType`, `F1000`, `Simulated`, `DREAM5`} contain a `RunAll.sh` that launches the learning of the different models, providing that the required librairies and packages are installed. The directory `scripts/Analysis` contains a script that generates the figures of the article once the results have been obtained. The two directories `GNN_keras` and `GNN_spektral` contain the script of the models, the latter being largely based on the initial implementation of https://gitlab.gwdg.de/UKEBpublic/graph-lrp [Chereda et al., Genome Medicine, 2021] (released under MIT license, which is included in the directory as well).

Results presented in the article are based on the following software & package versions:

- Python 3.8.10
- scikit-learn 1.0.1
- Tensorflow 2.7.0
- Keras 2.7.0
- Spektral 1.0.8

all used with cudnn version 8.8.

- R 4.3.2 (based on BLAS 3.9.0 and LAPACK 3.9.0 libraries)
- randomForest 4.7-1.1
- e1071 1.7-9
- glmgraph 1.0.3 (taken from CRAN archives)
- ROCR 1.0-11
- pROC 1.18.0
- tidyr 1.1.4
- dplyr 1.0.7
- ggplot2 3.3.5
- RColorBrewer 1.1-2
- igraph 1.2.6
- profmem 0.6.0


## Authors

Céline Brouard, Université Fédérale de Toulouse, INRAE, MIAT, 31326 Castanet-Tolosan, France

Raphaël Mourad, Université Fédérale de Toulouse, INRAE, MIAT, 31326 Castanet-Tolosan, France and Université Paul Sabatier, 31062 Toulouse, France

Nathalie Vialaneix, Université Fédérale de Toulouse, INRAE, MIAT, 31326 Castanet-Tolosan, France

For information about the repository, please contact celine.brouard@inrae.fr or nathalie.vialaneix@inrae.fr.


## License

The content of this repository is released under GPL v3 (or later).
